# thea2Shattering_i18n_tools

Scripting tools to interact with Thea 2 The Shattering's files in order to translate them easily.

## Databases
To convert from XML

```bash
grep Key Database/game_files/DATABASE_UI_LOCALIZATION.xml | grep -v "UI_EMPTY" | grep -v '<!--<Entry' | awk -F'"' '{print "#. "$2"\n" "msgid " "\""$4"\"" "\n" "msgstr " "\"\""}' > /tmp/DATABASE_UI_LOCALIZATION.pot.tmp
dos2unix /tmp/DATABASE_UI_LOCALIZATION.pot.tmp
sed -i 's/\\E/\\\\E/' /tmp/DATABASE_UI_LOCALIZATION.pot.tmp
# Insert header
msgid ""
msgstr ""
"MIME-Version: 1.0\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: \n"
"Language-Team: \n"
msguniq --no-wrap /tmp/DATABASE_UI_LOCALIZATION.pot.tmp > Database/pot/DATABASE_UI_LOCALIZATION.pot
```

To rollback from po:

```bash
./rollback_database.sh Database/game_files/DATABASE_UI_LOCALIZATION.xml Translation/fr/po/Database/DATABASE_UI_LOCALIZATION.po fr
./rollback_database.sh Database/game_files/DATABASE_QUEST_LOCALIZATION.xml Translation/fr/po/Database/DATABASE_QUEST_LOCALIZATION.po fr
./rollback_database.sh Database/game_files/DATABASE_DES_LOCALIZATION.xml Translation/fr/po/Database/DATABASE_DES_LOCALIZATION.po fr
```


## Modules

To convert from txt:

```bash
for text in Modules/game_files/*; do ./extract_module.sh "${text}"; done

```

To convert back from po:

```bash
echo "start at $(date)"; time for i in Translation/fr/po/Modules/*; do echo "rollback ${i}..."; time ./rollback_module.sh "${i}" fr; echo "done"; done; echo "end at $(date)";
```