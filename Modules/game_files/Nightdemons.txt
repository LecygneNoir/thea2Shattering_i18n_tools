-- [EVENT] --NightdemonsTradeadv(0)
+[NODE]2
[STORY]
The trader's stalls stand open for you


[/STORY]
[OUT]Trade.
[OUT]Trade.
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]7
[STORY]
'You are no friends of ours, so you will pay extra.'
[/STORY]
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]11
[STORY]
'You waste our time, go.'
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --NightdemonsVillage(1)
+[NODE]2
[STORY]
You enter the territory claimed by the night demons. You see werewolves and strigas skulk in the shadows, watching you with hungry eyes:

'Have you come to offer your tasty blood, or flesh maybe?'
[/STORY]
[OUT][Horz or Zorya] As masters of moon and the stars, claim your affinity with the children of the night.
[OUT]Ask if you can enter and explore?
[OUT]Leave.
[OUT][Friend or night demon] Say that you accept any dangers that may befall you here and would like to enter regardless.
[/NODE]

+[NODE]3
[STORY]
You enter the territory claimed by the night demons. You feel many eyes upon you and a deep growl greets you:

'Have you come to feed our bloodlust, enemy of the night?'


[/STORY]
[OUT]Attack!
[OUT]Attack!
[OUT]Leave.
[/NODE]

+[NODE]4
[STORY]
The inhabitants of this place begin circling you and you feel terribly exposed to their hunger.

'You carry much silver,  mithril, or other material that was borne of the faul metals that hurt us  and come here.  Leave.'
[/STORY]
[OUT]Better leave.
[OUT]Attack!
[OUT]Attack!
[/NODE]

+[NODE]7
[STORY]
'You are friends, for now. So, you may enter at your own peril.'
[/STORY]
[OUT]Ask around for anything of interest.
[OUT]Ask if they would trade with you?
[OUT]There are some werewolves in a fighting ring, go see them.
[OUT]Visit the pleasure houses.
[/NODE]

+[NODE]11
[STORY]
'I accept your offering and will summon the unliving to your service.'
[/STORY]
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]12
[STORY]
'You have proven friends to us so far, yes, we will trade with you.'
[/STORY]
[OUT]Trade.
[/NODE]

+[NODE]15
[STORY]
The night beasts are brawling to prove their worth. You are welcome to join if you dare.

Close by, a striga master is showing off his mystical powers.
[/STORY]
[OUT][One time only] Test your brawn against a werewolf. Two of your strongest will fight.
[OUT][One time only] Test your brawn against a werewolf.  One of your strongest will fight.
[OUT]You have either exhausted your chances for a challenge, or this just isn't your time. Go back.
[OUT][One time only] Challenge the striga master, one on one.
[/NODE]

+[NODE]16
[STORY]
'It is your lucky day, you may enter, at your own risk.'
[/STORY]
[OUT]Ask if they would trade with you?
[OUT]Visit the pleasure houses.
[OUT]The pleasure house is busy, come back later.
[/NODE]

+[NODE]18
[STORY]
You enter the dark rooms of the pleasure house and an exotic dancer that seems part spirit part flesh entertains you. 

Your spirt is restored but you feel some of your blood was drained and this left you ailing.
[/STORY]
[OUT]Thank them, rest and leave.
[/NODE]

+[NODE]19
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'... but you do feel renewed. 
[/STORY]
[OUT]Thank them, rest and leave.
[/NODE]

+[NODE]20
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'... You do not feel very well, but it was certainly a night to remember, or was it?
[/STORY]
[OUT]Leave...
[/NODE]

+[NODE]21
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'... And one wolf seems to now think you're its daddy.
[/STORY]
[OUT]Leave...
[/NODE]

+[NODE]22
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'... and when you look down at yourself - you're a striga!
[/STORY]
[OUT]Leave...
[/NODE]

+[NODE]28
[STORY]
The night demons are slain and you hear a dreadful howling in the distance...

But then again, you are chosen of Horz, an alpha of the moon. it is your right to claim these beasts if you so wish and you know those who witnessed it will respect you.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]31
[STORY]
The night beasts defeat you and claim their blood price for your attack.
[/STORY]
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]36
[STORY]
'No, leave. Return with those who are friends of the night, perhaps then we will listen.'
[/STORY]
[OUT]Better leave.
[OUT]Attack!
[OUT]Attack!
[/NODE]

+[NODE]37
[STORY]
'It is your lucky day, you call nature your master and we respect that. You may enter, at your own risk.'
[/STORY]
[OUT]Ask if they would trade with you?
[OUT]Visit the pleasure houses.
[/NODE]

+[NODE]38
[STORY]
From within the shadows they speak to you:

'You are chosen of the night. We want to be in your pack.'
[/STORY]
[OUT]Accept whoever it is that wants to join.
[OUT]Refuse politely.
[/NODE]

+[NODE]44
[STORY]
The night demons are slain and you hear a dreadful howling in the distance...
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]48
[STORY]
The werewolf yields to your strength and gives you trophies as reward for your victory. 

His pack howls in your honour.
[/STORY]
[OUT]Offer to drink with your opponent for a battle well fought.
[OUT]Ask for one of you to be turned into a child of the moon like him!
[OUT]Ask for one of you to be turned into a child of the moon like him!
[/NODE]

+[NODE]49
[STORY]
Night demons gather and bow before your challenge. A werewolf enters the ring and bares his razor-sharp teeth in an invitation.
[/STORY]
[OUT]Begin.
[/NODE]

+[NODE]50
[STORY]
You are defeated and must bow down to the victor. 

'You fought well, but you are yet be a true night child. Now, if you still stand, let us drink together!'
[/STORY]
[OUT]Drink if you still can...
[/NODE]

+[NODE]52
[STORY]
One of the challengers wakes with the next full moon as child of the night!
[/STORY]
[OUT]Praise Horz. 
[/NODE]

+[NODE]56
[STORY]
'Agreed, you deserve to join our ranks, for Horz!'

One of the challengers wakes with the next full moon as child of the night!
[/STORY]
[OUT]Praise Horz. 
[/NODE]

+[NODE]57
[STORY]
'You give up? Coward. Begone from my sight.'
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]58
[STORY]
Night demons gather and bow before your challenge. A werewolf enters the ring and bares his razor-sharp teeth in an invitation.
[/STORY]
[OUT]Begin.
[/NODE]

+[NODE]60
[STORY]
You are defeated and must bow down to the victor. 

'You fought well, but you are yet be a true night child. Now, if you still stand, let us drink together!'
[/STORY]
[OUT]Drink if you still can...
[/NODE]

+[NODE]62
[STORY]
One of the challengers wakes with the next full moon as child of the night!
[/STORY]
[OUT]Praise Horz. 
[/NODE]

+[NODE]63
[STORY]
The werewolf yields to your strength and gives you trophies as reward for your victory. 

His pack howls in your honour.
[/STORY]
[OUT]Offer to drink with your opponent for a battle well fought.
[OUT]Ask to be turned into a child of the moon like him!
[/NODE]

+[NODE]64
[STORY]
'Agreed, you deserve to join our ranks, for Horz!'

One of the challengers wakes with the next full moon as child of the night!
[/STORY]
[OUT]Praise Horz. 
[/NODE]

+[NODE]65
[STORY]
There is a necromancer staying in the settlement. They can offer you your very own unliving companion, for a price.
[/STORY]
[OUT]Offer some pets as payment (you need to have 15).
[OUT]Offer a child.
[OUT]Offer a party member, including adults.
[/NODE]

+[NODE]68
[STORY]
'Hmm, not my typical price, but I will accept it, this once.'
[/STORY]
[OUT]Pay.
[/NODE]

+[NODE]70
[STORY]
'You have enchanted my heart with your masterful skill. I bow to your superior power and your baffling beauty. 

Will you agree to me my mistress of the night? One sweet bite and your world will become changed forever!'
[/STORY]
[OUT][Elf] Alas, elves cannot accept such dark gifts. Thank him all the same.
[OUT]Accept the offer.
[OUT]Decline politely and leave.
[OUT]Accept the offer.
[/NODE]

+[NODE]71
[STORY]
'Challenge me? How delightful. Let us dance my dear.'
[/STORY]
[OUT]Begin.
[/NODE]

+[NODE]72
[STORY]
'Ah, yes, children of light. Such a shame, still. I am enamoured and so shall gift you a night-kiss so that it may remind you of me. From now on, you shall be part of our night pack, even if only a little. Now fare thee well, my love!'
[/STORY]
[OUT]Thank him and leave, this is getting awkward. 
[/NODE]

+[NODE]74
[STORY]
You become the mistress of the night. The striga master is exhausted from the ritual, so it is a good time to go.
[/STORY]
[OUT]Leave, but promise to visit...
[/NODE]

+[NODE]75
[STORY]
'Wonderful, yes, your power is great and I am impressed. I bow to your superiority, friend. Here, a reward for your challenge.'
[/STORY]
[OUT]Thank him and leave.
[OUT]Ask to be turned into a striga master!
[OUT]Ask to be turned into a common striga.
[/NODE]

+[NODE]76
[STORY]
'Yes, you shall become a brother of mine. Agreed.'
[/STORY]
[OUT]Thank him and leave.
[/NODE]

+[NODE]77
[STORY]
'Yes, you shall become a child of mine. Agreed.'
[/STORY]
[OUT]Thank him and leave.
[/NODE]

+[NODE]78
[STORY]
'And so, you bow down to my superior power, good...
[/STORY]
[OUT]Accept your defeat. 
[OUT]Accept your defeat. 
[OUT]Accept your defeat. 
[/NODE]

+[NODE]79
[STORY]
'Despite your failure, you have enchanted my heart  with your baffling beauty. You will be my mistress of the night. One sweet bite and your world will become changed forever!'

You become the mistress of the night. The striga master is exhausted from the ritual, so it is a good time to go.
[/STORY]
[OUT]You don't seem to have too much choice. Nod.
[/NODE]

+[NODE]81
[STORY]
'Also, you shall become a child of mine and serve the night.'
[/STORY]
[OUT]You don't seem to have too much choice. Nod.
[/NODE]

+[NODE]82
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'... and when you look down at yourself - you're a werewolf!
[/STORY]
[OUT]Leave...
[/NODE]

+[NODE]83
[STORY]
You got drunk in a night demon pleasure house, when you wake, you are covered in wolf fur and rather rough 'love bites'.
[/STORY]
[OUT]Leave...
[/NODE]

+[NODE]84
[STORY]
'You have proven friendly to us so far, yes, we will trade with you.'
[/STORY]
[OUT]Trade.
[/NODE]

+[NODE]85
[STORY]
'Yes, the night mates, bonded forever and you call one of them your alpha, good. Enter, enjoy the night!'
[/STORY]
[OUT]Ask around for anything of interest.
[OUT]Ask if they would trade with you?
[OUT]There are some werewolves in a fighting ring, go see them.
[OUT]Visit the pleasure houses.
[OUT]A few children of the night keep watching you. Approach them.
[/NODE]

+[NODE]87
[STORY]
'No deal, time to depart?'
[/STORY]
[OUT]Better leave.
[OUT]Visit the pleasure houses.
[OUT]There are some werewolves in a fighting ring, go see them.
[OUT]Ask if they would trade with you?
[/NODE]

+[NODE]90
[STORY]
Someone you once lost to the night demons has returned to you, albeit, changed.
[/STORY]
[OUT]Take them with you and leave.
[/NODE]

+[NODE]92
[STORY]
'Ah, it seems you were only able to become a lesser child of the night, still, we are bound now!'
[/STORY]
[OUT]Leave, but promise to visit...
[/NODE]

+[NODE]93
[STORY]
'Agreed, you deserve to join our ranks, for Horz!'

One of the challengers wakes with the next full moon as child of the night!
[/STORY]
[OUT]Praise Horz. 
[/NODE]

+[NODE]95
[STORY]
'Agreed.'
[/STORY]
[OUT]Trade.
[/NODE]

[/EVENT]

-- [EVENT] --ArmyNightDemon_(2)
+[NODE]2
[STORY]
Beasts of the night are stalking you!
[/STORY]
[OUT]Greet your allies. 
[OUT]Stay cautious, but speak to them.
[OUT]Use silver, or better metal, to intimidate them into backing down!
[OUT]Fight!
[OUT]Move away.
[/NODE]

+[NODE]3
[STORY]
'Apologies, friends. We did not recognise you. Greetings and well wishes.'
[/STORY]
[OUT]Ask if they want to trade.
[OUT]Leave.
[/NODE]

+[NODE]11
[STORY]
They do not answer, they keep coming towards you.
[/STORY]
[OUT]Leave.
[OUT]Use silver, or better metal, to intimidate them into backing down!
[OUT]Fight!
[OUT]Face the night beasts with spirit to gain their respect.
[/NODE]

+[NODE]17
[STORY]
You defeat the night beasts.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]19
[STORY]
The night beats defeat you and feast on your blood and life essence before you manage to escape.
[/STORY]
[OUT]Run.
[/NODE]

+[NODE]25
[STORY]
The night demons cower before you and skulk back into the shadows.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]28
[STORY]
You defeat the night beasts and they are impressed with your understanding of the mystical ways. 

[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

