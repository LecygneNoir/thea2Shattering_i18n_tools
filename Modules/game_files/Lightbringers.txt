-- [EVENT] --VillageSmall(0)
+[NODE]2
[STORY]
You come to a settlement claimed by the light. Its folk roam the streets in unison, working, crafting, ploughing the fields, all in silence, all together. 

When they spot you, they approach:

'You have come to join us?'
[/STORY]
[OUT]No, tell them you have come to talk and perhaps establish a friendship.
[/NODE]

+[NODE]3
[STORY]
'You are either one of us, or you are not. Come . . .'

Light wisps begin to circle you, their cold heat on your skin and burning icicles touching your guts in an attempt at joining.
[/STORY]
[OUT]Refuse and leave quickly.
[OUT][Turmoil or Light]This is an abomination that defies both turmoil (seeking to tame it) and light (abusing its power). Attack head on!
[OUT][Magic or Nature]This force seems immaterial. Call upon your domain and face these fiends with your spirit!
[OUT][Harmony or Intellect]Show them the strength of your mind, a will that will not be bent. 
[/NODE]

+[NODE]6
[STORY]
You destroy the lightbringers and the village burns, as if the light will not allow you a full victory. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]8
[STORY]
You are badly beaten and forced to flee. One or more of your folk could fall under their dominion . . .
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
Your resistance wakens something within the taken folk and they banish the light from their souls! Alas, their bodies are too weak and burnt out to survive without the hive mind and they fall dead, but free. 

The village itself burns as if the light would not abide your victory.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
'Bringer of light, come, see us. One day you will join. For now some us may wish to travel with you.'
[/STORY]
[OUT]See the light village.
[/NODE]

+[NODE]19
[STORY]
The village seems frozen in time. The people move so slowly you can barely see it. Yet they all seem really happy. 


[/STORY]
[OUT]Have your spirit healed.
[OUT]Have your mental health cured.
[OUT]Remove curses. 
[OUT]Recruit lightbringers.
[OUT]Nothing to do for now, leave.
[/NODE]

+[NODE]32
[STORY]
The lightbringers turn to you in anger, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --ArmyLight(1)
+[NODE]2
[STORY]
A group of folk taken by the light scourge approaches you - lightbringers, as they are now known.

They do not speak. They seek only to bring you into their commune with the light-mind.
[/STORY]
[OUT]Speak to them.
[OUT]Fight!
[OUT]Face them with your spirit!
[OUT]Move away.
[/NODE]

+[NODE]17
[STORY]
You destroy the lightbringers and their bodies turn to ash. 
[/STORY]
[OUT]Take their stuff and leave.
[/NODE]

+[NODE]19
[STORY]
Your spirit is too weak. The lightbringers assail you, and you barely escape - and perhaps some of you do not . . .
[/STORY]
[OUT]Run.
[/NODE]

+[NODE]27
[STORY]
'You, you gave more light. You will join when you are ready...'

They move away.
[/STORY]
[OUT]Attack!
[OUT]Leave.
[/NODE]

+[NODE]30
[STORY]
A group of the lightbringers approaches, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --VillageLarge(2)
+[NODE]2
[STORY]
You come to a settlement claimed by the light. Its folk roam the streets in unison, working, crafting, ploughing the fields, all in silence, all together. 

When they spot you, they approach:

'You have come to join us?'
[/STORY]
[OUT]No, tell them you have come to talk and perhaps establish a friendship.
[/NODE]

+[NODE]3
[STORY]
'You are either one of us, or you are not. Come . . .'

Light wisps begin to circle you, their cold heat on your skin and burning icicles touching your guts in an attempt at joining.
[/STORY]
[OUT]Refuse and leave quickly.
[OUT][Turmoil or Light]This is an abomination that defies both turmoil (seeking to tame it) and light (abusing its power). Attack head on!
[OUT][Magic or Nature]This force seems immaterial. Call upon your domain and face these fiends with your spirit!
[OUT][Harmony or Intellect]Show them the strength of your mind, a will that will not be bent. 
[/NODE]

+[NODE]6
[STORY]
You destroy the lightbringers and the village burns, as if the light will not allow you a full victory. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]8
[STORY]
You are badly beaten and forced to flee. One or more of your folk could fall under their dominion . . .
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
Your resistance wakens something within the taken folk and they banish the light from their souls! Alas, their bodies are too weak and burnt out to survive without the hive mind and they fall dead, but free. 

The village itself burns as if the light would not abide your victory.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
'Bringer of light, come, see us. One day you will join. For now some us may wish to travel with you.'
[/STORY]
[OUT]See the light village.
[/NODE]

+[NODE]19
[STORY]
The village seems frozen in time. The people move so slowly you can barely see it. Yet they all seem really happy. 


[/STORY]
[OUT]Have your spirit healed.
[OUT]Have your mental health cured.
[OUT]Remove curses. 
[OUT]Recruit lightbringers.
[OUT]Nothing to do for now, leave.
[/NODE]

[/EVENT]

-- [EVENT] --Village Very Small(3)
+[NODE]2
[STORY]
You come to a settlement claimed by the light. Its folk roam the streets in unison, working, crafting, ploughing the fields, all in silence, all together. 

When they spot you, they approach:

'You have come to join us?'
[/STORY]
[OUT]No, tell them you have come to talk and perhaps establish a friendship.
[/NODE]

+[NODE]3
[STORY]
'You are either one of us, or you are not. Come . . .'

Light wisps begin to circle you, their cold heat on your skin and burning icicles touching your guts in an attempt at joining.
[/STORY]
[OUT]Refuse and leave quickly.
[OUT][Turmoil or Light]This is an abomination that defies both turmoil (seeking to tame it) and light (abusing its power). Attack head on!
[OUT][Magic or Nature]This force seems immaterial. Call upon your domain and face these fiends with your spirit!
[OUT][Harmony or Intellect]Show them the strength of your mind, a will that will not be bent. 
[/NODE]

+[NODE]6
[STORY]
You destroy the lightbringers and the village burns, as if the light will not allow you a full victory. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]8
[STORY]
You are badly beaten and forced to flee. One or more of your folk could fall under their dominion . . .
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
Your resistance wakens something within the taken folk and they banish the light from their souls! Alas, their bodies are too weak and burnt out to survive without the hive mind and they fall dead, but free. 

The village itself burns as if the light would not abide your victory.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
'Bringer of light, come, see us. One day you will join. For now some us may wish to travel with you.'
[/STORY]
[OUT]See the light village.
[/NODE]

+[NODE]19
[STORY]
The village seems frozen in time. The people move so slowly you can barely see it. Yet they all seem really happy. 


[/STORY]
[OUT]Have your spirit healed.
[OUT]Have your mental health cured.
[OUT]Remove curses. 
[OUT]Recruit lightbringers.
[OUT]Nothing to do for now, leave.
[/NODE]

+[NODE]32
[STORY]
The lightbringers turn to you in anger, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Nest(4)
+[NODE]2
[STORY]
You come to a place claimed by the light. There are creatures and people here, all standing together, staring into space.

When they spot you, they approach:

'You have come to join us?'
[/STORY]
[OUT]No, tell them you have come to talk and perhaps establish a friendship.
[/NODE]

+[NODE]3
[STORY]
'You are either one of us, or you are not. Come . . .'

Light wisps begin to circle you, their cold heat on your skin and burning icicles touching your guts in an attempt at joining.
[/STORY]
[OUT]Refuse and leave quickly.
[OUT][Turmoil or Light]This is an abomination that defies both turmoil (seeking to tame it) and light (abusing its power). Attack head on!
[OUT][Magic or Nature]This force seems immaterial. Call upon your domain and face these fiends with your spirit!
[OUT][Harmony or Intellect]Show them the strength of your mind, a will that will not be bent. 
[/NODE]

+[NODE]6
[STORY]
You destroy the lightbringers and the nest burns, as if the light will not allow you a full victory. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]8
[STORY]
You are badly beaten and forced to flee. One or more of your folk could fall under their dominion . . .
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
Your resistance wakens something within the taken folk and they banish the light from their souls! Alas, their bodies are too weak and burnt out to survive without the hive mind and they fall dead, but free. 

The nest itself burns as if the light would not abide your victory.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
'Bringer of light, come, see us. One day you will join. For now we thank you for your aid.'
[/STORY]
[OUT]Leave them be.
[OUT]Attack!
[/NODE]

+[NODE]32
[STORY]
A group of the lightbringers approaches, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

