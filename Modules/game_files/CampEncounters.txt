-- [EVENT] --CampEvents(0)
[/EVENT]

-- [EVENT] --NightMares(1)
+[NODE]2
[STORY]
As you fall asleep at your campsite, your dreams become heavy and unsettling. As you toss and turn, you feel more and more unsettled as your deepest, darkest fears become real and hold you trapped within the nightmare.
[/STORY]
[OUT]Try to wake up.
[/NODE]

+[NODE]4
[STORY]
You slowly wake and in the state of half-waking, you see mares sitting on your chests, siphoning your spirits.

As soon as you regain full consciousness, the demons are gone, but you still feel a deep chill run down your spines.
[/STORY]
[OUT]Get it together and move on.
[/NODE]

+[NODE]5
[STORY]
You try to wake and in the state of half-waking, you see mares sitting on your chests, siphoning your spirits.

The surge of panic feeds the demons and you fall back into tormented slumber, knowing they will have their way.

When morning comes, you are released, but you are wrecked and exhausted.
[/STORY]
[OUT]Get it together and move on.
[/NODE]

+[NODE]11
[STORY]
You try to wake and in the state of half-waking, you see mares sitting on your chests, siphoning your spirits.

The surge of panic feeds the demons and you fall back into tormented slumber, knowing they will have their way.

For one of you, this night will be the last in the realms of mortal flesh.
[/STORY]
[OUT]Get it together and move on.
[/NODE]

[/EVENT]

-- [EVENT] --Ignis fatuus(2)
+[NODE]2
[STORY]
As you sit by the campfire, some of the sparks from the flame begin to dance in the air. They move away from the bonfire and become glimmers in the night. 

They seem to want you to follow them somewhere.
[/STORY]
[OUT]Follow the glimmers.
[OUT]Follow the glimmers.
[OUT]Follow the glimmers.
[OUT]Stay put.
[/NODE]

+[NODE]3
[STORY]
You follow the glimmers for a short while and find yourself unexpectedly stuck in a bog!

The lights turned out to be flickers, mischievous little buggers always looking for a chance to lead people astray.

You get out, but you're tired and catch a cold.
[/STORY]
[OUT]Get back to camp.
[/NODE]

+[NODE]4
[STORY]
The glimmers lead you to an old crypt.
[/STORY]
[OUT]Say a prayer to Nyia, the goddess of death, for the souls to be at rest. Then search this place.
[OUT]Say a prayer to Nyia, the goddess of death, for the souls to be at rest. Then search this place.
[OUT]Say a prayer to Nyia, the goddess of death, for the souls to be at rest. Then search this place.
[/NODE]

+[NODE]5
[STORY]
The glimmers guide you to a path that is not only abundant in fruit, but also may prove a useful shortcut on your journey onwards. 

[/STORY]
[OUT]Get back to camp.
[/NODE]

+[NODE]8
[STORY]
Your prayer does not satisfy the angered spirits that dwell here still. 

The wraiths slowly materialise before you, seeking to take your souls.
[/STORY]
[OUT]Attack!
[OUT][Goblin Shaman or Wraith]Convince the wraiths to allow you to set them free.
[OUT][Magic users]Battle the wraiths with your spirit.
[OUT]Get back to camp -- this seems too dangerous.
[/NODE]

+[NODE]11
[STORY]
You defeat the wraiths and they dissolve into nothingness. A few linger and some may even stay with you, but they now pose no danger.

You're free to search this site in peace.
[/STORY]
[OUT]Gather what you can and leave.
[/NODE]

+[NODE]12
[STORY]
You are defeated and feel your spirit weaken as you are forced to flee.
[/STORY]
[OUT]Run back to your camp.
[/NODE]

+[NODE]18
[STORY]
Your prayer clearly appeased the spirits here, as you feel the air lighten and sense the passing of trapped souls. There is a chance one such soul may join you.

You are free to search this site now.
[/STORY]
[OUT]Gather what you can and leave.
[/NODE]

+[NODE]20
[STORY]
Your prayer clearly appeased the spirits here, as you feel the air lighten and sense the passing of trapped souls. There is a chance one such soul may join you.

You are free to search this site now.
[/STORY]
[OUT]Gather what you can and leave.
[/NODE]

+[NODE]23
[STORY]
The wraiths stop and nod in agreement. You perform the prayer to Nyia once more and this time, the power of the goddess flows through you and engulfs the spirits in its dark, empty embrace.

A few linger and some may even stay with you, but they now pose no danger.
[/STORY]
[OUT]Gather what you can and leave.
[/NODE]

[/EVENT]

-- [EVENT] --StrangeChest(3)
+[NODE]2
[STORY]
As you clear the land to make a better campsite, you uncover a heavy ironclad chest, likely unearthed here by the Shattering. It is bound in chains and has some odd foreign symbols on it.
[/STORY]
[OUT][Runemaster]These are no symbols, they are ancient runes -- not dwarven, but certainly trying to imitate your kin's craft. Try to decipher these runes.
[OUT][Wraith]You sense a fellow wraith trapped within this object. Take a closer look. 
[OUT][Magic user or Magic]Strong magic resides in this object. If you are to set up camp here, you'd better deal with it first. Take a closer look.
[OUT][Wisdom]Try to figure out what this writing may mean.
[OUT]Open the chest.
[OUT]Leave it be and set up camp a bit farther away.
[/NODE]

+[NODE]4
[STORY]
You remove the chains, but the chest remains closed. You hear a slow, malicious hissing. 
[/STORY]
[OUT]Attempt to carefully extract whatever is inside without further damaging the chest and hopefully before the hissing gets worse.
[OUT]Attempt to carefully extract whatever is inside without further damaging the chest and hopefully before the hissing gets worse.
[OUT]Try to smash it quickly and grab what you can before whatever is hissing comes your way.
[OUT]Try to smash it quickly and grab what you can before whatever is hissing comes your way.
[/NODE]

+[NODE]6
[STORY]
These runes are meant to work as protection for whatever lies within the chest. They are binding runes, so opening the chest will release the guardians -- most likely, wraiths. 

The chains work as a trigger. When broken, they will begin the process of releasing the wraiths.
[/STORY]
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Now that you know what this is, try to perform a cleansing ritual to release the wraiths, thus making it safe to open the chest.
[OUT][Wraith]Remove the chains and speak to your kind to calm them. Convince them that they don't need to follow whatever force bound them here. Instead, they can be free!
[/NODE]

+[NODE]7
[STORY]
You carefully chip away at the side of the chest and create an opening without touching the chains or breaking the runes. 

[/STORY]
[OUT]Grab what you can and leave. 
[/NODE]

+[NODE]8
[STORY]
You carefully chip away at the side of the chest and create an opening. Alas, you accidentally break one of the runes, and the chains break without you even touching them. 

Wraiths appear and descend upon you!

[/STORY]
[OUT]Fight!
[OUT][Wraith]Speak to your kind to calm them. Convince them that they don't need to follow whatever force bound them here. Instead, they can be free!
[OUT]Abandon the chest and try to flee.
[OUT]Abandon the chest and try to flee.
[/NODE]

+[NODE]10
[STORY]
You smash the chest, and a pack of angered wraiths appears before you and attacks!
[/STORY]
[OUT]Fight!
[OUT]Attack them with your willpower.
[OUT]Grab what you can and run for it. 
[/NODE]

+[NODE]17
[STORY]
The wraiths are no more, and now you are free to loot the chest safely.
[/STORY]
[OUT]Gather what is inside and leave.
[/NODE]

+[NODE]19
[STORY]
You try to carefully extract whatever is in the chest without doing further damage, but you fail. The thing shatters in your hands, releasing a pack of angered wraiths before you!
[/STORY]
[OUT]Fight!
[OUT]Attack them with your willpower.
[OUT]Grab what you can and run for it. 
[/NODE]

+[NODE]20
[STORY]
You smash the chest and can grab some loot quickly. 

But you see a pack of angered wraiths forming before you.
[/STORY]
[OUT]Fight!
[OUT]Attack them with your willpower.
[OUT]Grab what you can and run for it. 
[OUT]Grab what you can and run for it. 
[/NODE]

+[NODE]21
[STORY]
You carefully break away the side of the chest without causing further damage, and you grab some of the stuff from the inside. 

The hissing gets louder, however, and you see wraiths forming before you.

[/STORY]
[OUT]Fight!
[OUT]Attack them with your willpower.
[OUT]Grab what you can and run for it. 
[OUT]Grab what you can and run for it. 
[/NODE]

+[NODE]26
[STORY]
The ancient protection spell proved too powerful for you. The wraiths assault your spirit and force you to flee before you are lost entirely.
[/STORY]
[OUT]Run.

[/NODE]

+[NODE]30
[STORY]
These runes are meant to work as protection for whatever lies within the chest. They are binding runes, so opening the chest will release the guardians -- most likely, wraiths. 

The chains work as a trigger. When broken, they will begin the process of releasing the wraiths.

Luckily, you know how to break the protection safely.
[/STORY]
[OUT]Open the chest and take what is inside.
[/NODE]

+[NODE]32
[STORY]
'Yes, thank you! We are free now... or can we follow you, perhaps?'

An argument ensues among the wraiths. Most disappear, but some may choose to stay with you instead. 

You are free to loot the chest safely now.
[/STORY]
[OUT]Gather what is inside and leave.
[/NODE]

+[NODE]35
[STORY]
The wraiths listen. Some look interested, but others are too far gone, angry and bewildered by centuries of captivity. The two groups fight among each other. You feel your own spirit assaulted and you are forced to flee.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]37
[STORY]
The ritual worked, and the wraiths are free of their bonds. The chest is now safe for looting. 

[/STORY]
[OUT]Gather what is inside and leave.
[/NODE]

+[NODE]45
[STORY]
These runes are meant to work as protection for whatever lies within the chest. They are binding runes, so opening the chest will release the guardians -- most likely, wraiths. 

The chains work as a trigger. When broken, they will begin the process of releasing the wraiths.
[/STORY]
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Now that you know what this is, try to perform a cleansing ritual to release the wraiths, thus making it safe to open the chest.
[OUT][Wraith]Remove the chains and speak to your kind to calm them. Convince them that they don't need to follow whatever force bound them here. Instead, they can be free!
[/NODE]

+[NODE]46
[STORY]
These runes are meant to work as protection for whatever lies within the chest. They are binding runes, so opening the chest will release the guardians -- most likely, wraiths. 

The chains work as a trigger. When broken, they will begin the process of releasing the wraiths.
[/STORY]
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Despite the magic protection, the chest is very old and has many weak spots. Try to open the chest without breaking the chains.
[OUT]Now that you know what this is, try to perform a cleansing ritual to release the wraiths, thus making it safe to open the chest.
[OUT][Wraith]Remove the chains and speak to your kind to calm them. Convince them that they don't need to follow whatever force bound them here. Instead, they can be free!
[/NODE]

[/EVENT]

-- [EVENT] --Night attack(4)
+[NODE]2
[STORY]
You are rudely awakened by a rabble of folk slowly descending upon your campsite. At first, they seem like mere bandits, but you see their eyes are bloodshot, their faces twisted in an angry grimace, and their mouths foaming. 
[/STORY]
[OUT]Attack!
[OUT][Scavenger]Take a closer look.
[OUT][Harmony and intellect]They seem ailing, yet still human. Perhaps there is a way to help them rather than kill them. Try capturing them alive.
[OUT]Take a risk and find another way to deal with them.
[OUT]Grab whatever you can in haste and run away.
[OUT]Grab whatever you can in haste and run away.
[OUT]Grab whatever you can in haste and run away.
[/NODE]

+[NODE]7
[STORY]
You kill all the scavengers. Their bodies are covered with boils and seeping with odd green ooze. 

[/STORY]
[OUT]Get what you can off them safely and leave.
[OUT]Investigate the bodies.
[OUT]Bury the bodies and leave.
[/NODE]

+[NODE]8
[STORY]
The scavengers overwhelm you and force you to flee, abandoning some of your goods. 

You also feel sick afterwards. They must have infected you with something.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]9
[STORY]
The scavengers lie dead. You were lucky enough not to get infected by their strange illness. 

A couple of their pets may want to tag along with you, but they may be infected.
[/STORY]
[OUT]Take the pets.
[OUT]Kill the pets to be safe.
[/NODE]

+[NODE]11
[STORY]
The scabs and boils are so bad that these folks should have died long before now. You suspect an unnatural illness, and thus you know the bodies must be decapitated, hands and feet bound, and then burned to make sure they stay dead.
[/STORY]
[OUT]Perform the special rite and leave.
[/NODE]

+[NODE]25
[STORY]
They are your kin, but far gone with the dark sickness, a remnant of the age of darkness past. If you could capture them, perhaps they could yet be cured, as you have been by following the gods. 

But beware -- their madness is more than mental, for they spread toxin and disease too.
[/STORY]
[OUT]Try capturing them.
[OUT]Lure them into a trap.
[/NODE]

+[NODE]27
[STORY]
They seem ailing, yet still human. Perhaps there is a way to help them rather than kill them.
[/STORY]
[OUT]Try capturing them by force.
[OUT]Lure them into a trap.
[/NODE]

+[NODE]29
[STORY]
You capture the scavengers safely and, indeed, you find they are feverish and sick. 

[/STORY]
[OUT][Medic]Use medicine to help them out.
[OUT][Magic user]This illness reeks of darkness. Prepare a ritual to try to cleanse their minds.
[OUT][Light]Your domain can ease their pain.
[OUT][Elf]You have witnessed this dark taint upon your own kind as well. Use your spirit and your connection to the light to help them.
[OUT]Do your best to help them out. Use your connection to the gods to try to light their dark path.
[/NODE]

+[NODE]30
[STORY]
Not all minds and bodies are strong enough to survive this taint, and even with your help, some of the victims pass away. 

But others show signs of recovery, or at least improvement. They regain some semblance of their mind and are thankful for your aid. 

Perhaps some will choose to stay with you. Even if they do not, they leave what they can in thanks.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]31
[STORY]
Light surges through the ailing bodies and decayed souls, and you feel them slowly healing. 

For some, the journey may yet prove too much. But you have given them a chance. The scavengers regain some semblance of their mind and are thankful for your aid. 

Perhaps some will choose to stay with you. Even if they do not, they leave what they can in thanks.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]34
[STORY]
Some of the disease spreads to you, but nothing life-threatening. 

You do your best to help, but most of the victims are too far gone and die. The few who survive show signs of recovery, or at least improvement. They regain some semblance of their mind and are thankful for your aid. 

Perhaps some will choose to stay with you. Even if they do not, they leave what they can in thanks.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]35
[STORY]
Some of the disease spreads to you, but nothing life-threatening. You do your best to help, but the victims are too far gone and die. You know that you have made their deaths more peaceful at the very least, and for that, you feel relief.  

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]38
[STORY]
Some of the disease spreads to you, and you fail to properly capture the scavengers. Many run away, and those you catch are too far gone to be helped, and die.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]42
[STORY]
They seem ailing, yet still human. Perhaps there is a way to help them rather than kill them.
[/STORY]
[OUT]Try capturing them by force.
[OUT]Lure them into a trap.
[/NODE]

+[NODE]48
[STORY]
Not all minds and bodies are strong enough to survive this taint, and even with your help, some of the victims pass away. 

But others show signs of recovery, or at least improvement. They regain some semblance of their mind and are thankful for your aid. 

Perhaps some will choose to stay with you. Even if they do not, they leave what they can in thanks.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Werwolf attack(5)
+[NODE]2
[STORY]
As you set up camp at night, you hear the howling of wolves around you. Some of it is worryingly guttural, almost manlike.
[/STORY]
[OUT][Night demon]Call out into the night.
[OUT][Horz]Demand obedience from these night hunters in the name of the moon lord himself.
[OUT][Hunter]Listen to the howls; there seems to be more happening here than an attack.
[OUT][Wisdom]You have some silver. Use it to create a protective circle.
[OUT]Gather your things quickly and try to run.
[OUT]Stand firm and fight!
[/NODE]

+[NODE]4
[STORY]
The pack bows down in respect of the night lord. One of the lesser wolves considers staying with you.

A lone werewolf walks into view. He is holding his side and you see a silver dagger lodged in it. He collapses before you.

[/STORY]
[OUT]Take the dagger out and try to help him.
[OUT]Let him die and loot the body after. 
[/NODE]

+[NODE]7
[STORY]
You slay the beasts and their bodies shift to human form. A stray wolf, now without a pack, stays to follow you.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
The werewolves are agitated by your efforts and charge!
[/STORY]
[OUT]Defend.
[OUT]One of us will stay behind and sacrifice their life to save the rest!
[OUT]Leave one child behind, and maybe they will let the rest of us go!
[/NODE]

+[NODE]19
[STORY]
The pack retreats, leaving you to help the wounded werewolf. 

It has a silver blade lodged in its side, and the wound is festering with an odd taint. 
[/STORY]
[OUT]Help the werewolf.
[OUT]Leave it to die.
[/NODE]

+[NODE]24
[STORY]
The brave soul charges the pack of werewolves. The creatures respect the warrior's honour and do not chase the rest of you.
[/STORY]
[OUT]Keep running.
[OUT]Keep running.
[/NODE]

+[NODE]25
[STORY]
You leave the crying child behind and run.
[/STORY]
[OUT]Keep running.
[OUT]Keep running.
[OUT]Keep running.
[OUT]Keep running.
[/NODE]

+[NODE]28
[STORY]
Your brave friend returns to you, changed, but ever loyal!
[/STORY]
[OUT]Great, welcome them.
[/NODE]

+[NODE]29
[STORY]
The werewolves seem even more infuriated by your barbaric ways and they attack viciously, growling: 

'Filthy beasts, sacrificing your own young!'

You run, but not without getting mauled badly.
[/STORY]
[OUT]Keep running.
[/NODE]

+[NODE]32
[STORY]
You run away, but many moons later, a wolf -- one of the children -- comes back to its home, and despite your abandonment, wants to stay with you.
[/STORY]
[OUT]Great, welcome them.
[/NODE]

+[NODE]33
[STORY]
The howling quiets, and a lone werewolf walks into view. He is holding his side, and you see a silver dagger lodged in it. He collapses before you.
[/STORY]
[OUT]Take the dagger out and try to help him.
[OUT]Let him die and loot the body after. 
[/NODE]

+[NODE]34
[STORY]
You hear the wolves circling you, but they do not attack. 

The pack is clearly angered, yet also desperate. As you observe, you notice a werewolf among them, wounded, perhaps dying, yet still distrustful of your presence.
[/STORY]
[OUT]Attack!
[OUT]Stay safe within the silver circle and wait for them to leave.
[OUT][Animal kinship]Try to show that you wish to help the wounded beast. 
[/NODE]

+[NODE]35
[STORY]
You hear the wolves circling you, but they do not attack. 

The pack is clearly angered, yet also desperate. As you observe, you notice a werewolf among them, wounded, perhaps dying, yet still distrustful of your presence.
[/STORY]
[OUT]Attack!
[OUT][Wisdom]You have some silver. Use it to create a protective circle.
[OUT][Animal kinship]Try to show that you wish to help the wounded beast. 
[OUT]These are no mere beasts, yet they are still guided by instinct. The werewolf is wounded, and the pack seeks help. Try to convince them that you mean no harm and will try to aid them by throwing away some silver - a gesture of good will.
[/NODE]

+[NODE]36
[STORY]
You remain within the circle, and eventually the pack skulks away into the darkness. It was a restless night, but your resolve strengthens.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]40
[STORY]
The werewolf pack respects that you yield to their power, and they leave you be without any further harm. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]43
[STORY]
The dagger proves more than just silver. It was clearly cursed, as it turns to ash when you take it out. You are struck by remnants of a spell, but no curse lingers on you.

Your healer tends to the wounds. The werewolf should recover in no time.
[/STORY]
[OUT]Good. Leave him for the pack to reclaim.
[/NODE]

+[NODE]44
[STORY]
The pack howls in thanks, and you find a stash left for you as a reward.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]45
[STORY]
The dagger proves more than just silver. It was clearly cursed, as it turns to ash when you take it out. You are struck by remnants of a spell, and a curse lingers upon you too.

You do your best with the creature's wounds. The werewolf should recover in time.
[/STORY]
[OUT]Good. Leave him for the pack to reclaim.
[/NODE]

+[NODE]46
[STORY]
The dagger proves more than just silver. It was clearly cursed, as it turns to ash when you take it out. You are struck by remnants of a spell, but no curse lingers on you.

You do your best with the creature's wounds. The werewolf should recover in time.
[/STORY]
[OUT]Good. Leave him for the pack to reclaim.
[/NODE]

+[NODE]48
[STORY]
You wait until the werewolf dies. The pack howls in the darkness from despair, but for some reason they do not attack you.

You soon discover that the dagger that was lodged in the werewolf's side was in fact cursed, and some of its ill effects may reach you.
[/STORY]
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]51
[STORY]
The pack snarls and paces around you, not willing to trust a stranger. 

You do, however, have a window to escape now.
[/STORY]
[OUT]Run.
[OUT]Attack!
[/NODE]

+[NODE]55
[STORY]
You are strong and agile, so you outrun the wolves, although it is exhausting.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]57
[STORY]
You outrun the wolves, although it is exhausting.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]58
[STORY]
You outrun the wolves, although it is exhausting, and you have to drop some bags on the way.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]61
[STORY]
You are badly beaten and forced to flee and when the next full moon rises, you realise you were cursed to walk as a creature of the night!
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

