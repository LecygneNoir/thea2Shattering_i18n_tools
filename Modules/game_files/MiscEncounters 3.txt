-- [EVENT] --Haunted wife(0)
+[NODE]2
[STORY]
You come across a small hamlet. Within, you find a group of worried-looking folk gathered around a crying woman.
[/STORY]
[OUT]Ask them what's wrong.
[OUT]Use this distraction to attack them!
[OUT]None of your business, really; just leave.
[/NODE]

+[NODE]3
[STORY]
The woman turns to you, hesitant, then looks to others for courage and they nod, so she speaks:

'It's that useless lump of a husband of mine. He won't leave me alone!'
[/STORY]
[OUT]Ask her why the others will not help. Surely one man isn't that dangerous?
[/NODE]

+[NODE]4
[STORY]
'The bugger had to hold on to grass when he shat, so no, it's not that. He's a dead one, you see. He passed two weeks gone. But he keeps comin' my way, climbing into my bed as if nothin' happened!'
[/STORY]
[OUT][Zerca or wisdom]Tell her you know of a way to be rid of the apparition, for it is bound to her by marital attachment for sure. Tell her to wear seven dresses.
[OUT][Shaman]Tell her spirits are your expertise, and you will speak to this husband of hers.
[OUT]Say that you are happy to kill the ghost if she likes -- for a price, of course.
[/NODE]

+[NODE]5
[STORY]
'So, you say I should put on seven of my best frocks, then go to sleep as always? 

Seems an odd thing, and I'll have to borrow frocks. You sure about this?'
[/STORY]
[OUT]Nod in confirmation.
[OUT]Nod in confirmation.
[/NODE]

+[NODE]6
[STORY]
The woman reports the next day:

'Smart one, you are! I did as you asked, wore my seven frocks, and the bugger came and he stands there gawking at me. Seven frocks? - says he with his stupid mouth open wide. Seven, say I.'
[/STORY]
[OUT]Wait to hear the rest.
[/NODE]

+[NODE]7
[STORY]
'He frowns at me - Why, I never saw no woman wearing seven frocks before!

Why, I never saw a dead husband come into the marital bed before! - say I. And the wretched thing, stumped, goes and disappears!'

She gives you her family heirloom as reward.
[/STORY]
[OUT]Tell her you were happy to help.
[/NODE]

+[NODE]8
[STORY]
The woman looks unsure.

'You ghost-meddling folk, you worry me as much as my deadbeat husband.'
[/STORY]
[OUT]Say that in that case, you wish her many more nights with her beloved.
[OUT]Shrug and say that either she'll take your help, pay for it and be done with it, or you'll happily leave.
[OUT]In that case, just kill those ungrateful bastards!
[OUT]Ask why she is so set against spirits. It is still her husband, no?
[/NODE]

+[NODE]9
[STORY]
'No need to get all bothered, sirs. I meant no offendin'. Kill fire with fire's what I say. 

You go speak to that husband of mine. Night is comin', and he will come too.'
[/STORY]
[OUT]Agree and go into her house to talk to the spirit.
[/NODE]

+[NODE]10
[STORY]
'Kill a spirit, will ya? I say good. My husband is gone and done. This thing is no kin of mine. You be careful, though, these things can bring fellow nasties when attacked. It's why we haven't done it ourselves yet.'

The rest of the villagers nod in agreement.
[/STORY]
[OUT]Stake out the house and attack the ghost when it appears.
[OUT]None of your business, really; just leave.
[/NODE]

+[NODE]12
[STORY]
You slay the wayward husband-ghost, and the wife throws herself in your arms:

'Thank you! I don't have much, but here's some food I made, warm and tasty. Just what you need after fightin'.'
[/STORY]
[OUT]Ask for a better reward.
[OUT]Ask for a better reward.
[OUT]Thank her and move on.
[/NODE]

+[NODE]13
[STORY]
'You cheeky buggers! Help, they say, but our livelihoods is all they want...

Here, have some gold and choke on it.'

You get some gold coins, but over the next few days, your throat gets sore and swollen as if you were about to choke.
[/STORY]
[OUT]Damned witch.
[/NODE]

+[NODE]15
[STORY]
'You cheeky buggers! Help, they say, but our livelihoods is all they want...

Here, have some gold and choke on it.'

[/STORY]
[OUT]Good, take the gold and leave.
[/NODE]

+[NODE]16
[STORY]
The husband-ghost is ferocious and chases you away from his house!

The villagers curse you out too, for you only angered the spirit.
[/STORY]
[OUT]Best leave quickly then.
[/NODE]

+[NODE]19
[STORY]
You slay all the settlers without mercy!

Their hamlet was poor and barely sustainable, but you find some resources you can take. You also find some young children hiding in one of the houses.
[/STORY]
[OUT]Kill them too. No need for enemies later.
[OUT]Take them in. They are young and will forget.
[OUT]Leave them in a nearby grove for forest demons to take. Some may get lucky and become something good, and others, well, at least you'll give them a chance.
[/NODE]

+[NODE]20
[STORY]
You slay all the settlers without mercy!

Their hamlet was poor and barely sustainable, but you find some resources you can take.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]21
[STORY]
The angry mob beats you badly, but they do not pursue as you run away.
[/STORY]
[OUT]Keep running.
[/NODE]

+[NODE]22
[STORY]
'It's unnatural's what it is! And yeah, sure, we live in a place where there be demon folk round every corner, but how's I supposed to know what is evil and what is not? Best just keep away from it all.'
[/STORY]
[OUT]Try to convince her that spirits can be good too and that she should give her husband a chance.
[OUT]Agree that it is never a clear thing, and offer to solve this problem for her by talking to the ghost.
[/NODE]

+[NODE]24
[STORY]
The husband-ghost is a lean, tired, middle-aged man with a bulging head who looks frail and sickly. 

He stares at you blankly, waiting for his wife to come to bed.

[/STORY]
[OUT]Ask him why he keeps returning here.
[/NODE]

+[NODE]25
[STORY]
'I was told by my missus to always come home to her bed else she'd hang my precious on the fence. 

You seen her mean face? She do it, she do it even in my death. I ain't losin' my preciousness!'
[/STORY]
[OUT]This ghost will be hard to convince, but try to do so anyway.
[OUT]Poor thing, just leave him be.
[OUT]Seems unlikely to leave quietly, so kill him.
[/NODE]

+[NODE]26
[STORY]
You leave the ghost be and while the wife is not happy, she does not say anything.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]28
[STORY]
'Oh, so my, my things are not, I mean she can't, oh! Great! Finally, I am free, thank you!'

The ghost disappears and the wife comes in to see your work done.
[/STORY]
[OUT]Tell her she is freed from the ghost.
[/NODE]

+[NODE]29
[STORY]
The wife cries out at night. You run into the bedroom and see the ghost ripping her apart with spectral claws.

Once she is gone, the spirit dissolves.
[/STORY]
[OUT]Tell the villagers there was no other way. The ghost was too attached to his wife, so he took her with him.
[OUT]Try to apologise for your error. Mistakes happen, and ghost whispering isn't a precise art.
[/NODE]

+[NODE]31
[STORY]
'Thank you! I don't have much, but here's some food I made, warm and tasty. Just what you need after fightin'.'
[/STORY]
[OUT]Ask for a better reward.
[OUT]Ask for a better reward.
[OUT]Thank her and move on.
[/NODE]

+[NODE]33
[STORY]
'You stop spillin' that filth from your mouth! I ain't sharing my bed with no ghost!'
[/STORY]
[OUT]Just leave her be, then. Not your business.
[OUT]Offer to speak to the ghost instead.
[/NODE]

+[NODE]34
[STORY]
'Well, he was a bugger, and drunk, but he was mine. I guess I'll give him another go. Thank you for helpin'. Here, have a hot dinner I made - I ain't got much more.''
[/STORY]
[OUT]Ask for a better reward.
[OUT]Ask for a better reward.
[OUT]Thank her and move on.
[/NODE]

+[NODE]36
[STORY]
The villagers are bewildered, but they know better than to question the words of a wise one, especially now that the problem is gone.

They offer you food in thanks, and you get the feeling that asking for more may be pushing it.
[/STORY]
[OUT]Take the food and leave.
[/NODE]

[/EVENT]

-- [EVENT] --HW Children(1)
+[NODE]2
[STORY]
You are woken by a scream and find that a child stands atop one of your teammates, holding a bloodied knife and about to plunge the deadly blow!

It is one of the children you 'rescued' from a settlement after killing all its folk.
[/STORY]
[OUT]Try to talk this kid down. Explain that you had to kill those villagers because they were weakened by demons.
[OUT]Intimidate the child. Say that they may get lucky and kill one person, but they will surely die next!
[OUT]Attack the child before it can do harm!
[OUT]Attack the child before it can do harm!
[/NODE]

+[NODE]3
[STORY]
A deformed, childlike creature creeps up on you as you sleep. 

It croaks:

'You, you did this to me, you monster! You killed my ma and pa and left me for them demons, and now I am like this!'

It lunges at you!
[/STORY]
[OUT]Defend yourself.
[OUT]Try to run away.
[OUT]Try to convince the child to calm down. Say that you made a mistake and would like them to stay with you and be safe and loved!
[/NODE]

+[NODE]5
[STORY]
'I...I am sorry. I didn't mean to. Please don't hurt me like you hurt my pa...please...'
[/STORY]
[OUT]Say that you will not do them harm as long as they behave from now on.
[OUT]Kill the child. It is an enemy waiting to strike again.
[/NODE]

+[NODE]8
[STORY]
'You KILLED my pa, you monster!'

The child stabs wildly and although they have little strength, much blood is drawn.
[/STORY]
[OUT]Grab the child by force and restrain them, but allow them to live on in the hope that they will calm down in time.
[OUT]Kill the child. It is an enemy waiting to strike again.
[/NODE]

+[NODE]9
[STORY]
'You KILLED my pa, you monster!'

The child stabs wildly and although they have little strength, much blood is drawn.
[/STORY]
[OUT]Grab the child by force and restrain them, but allow them to live on in the hope that they will calm down in time.
[OUT]Kill the child. It is an enemy waiting to strike again.
[/NODE]

+[NODE]11
[STORY]
You slay the demon child before they are able to utter a curse upon you in full.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
'I should leave you to the bies to devour your flesh, just like you left me.

Instead, I curse you and all your kin!'

The creature disappears, but a curse lingers upon your souls. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]17
[STORY]
'You KILLED my pa, you monster!'

The child stabs wildly, driven even more ferocious by your intimidation attempts.
[/STORY]
[OUT]Grab the child by force and restrain them, but allow them to live on in the hope that they will calm down in time.
[OUT]Kill the child. It is an enemy waiting to strike again.
[/NODE]

+[NODE]22
[STORY]
You let the wronged child live, even after they attacked you viciously and proclaimed revenge upon your kin.
[/STORY]
[OUT]Try to watch out for them in the future.
[OUT]Try to watch out for them in the future.
[/NODE]

+[NODE]23
[STORY]
Your commitment to care for the youth made them forgive your past, and they have truly become your own. 

One day, they bring you a treasure chest, proclaiming that it was their family's secret, but now you are their family so you should have it.
[/STORY]
[OUT]Great. 
[/NODE]

+[NODE]24
[STORY]
Despite your efforts to keep watch, one day you find you were robbed and the child is gone!
[/STORY]
[OUT]Damn.
[/NODE]

+[NODE]27
[STORY]
You run away, but the creature spits burning mud at you, leaving you scarred.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]29
[STORY]
The creature stops and stares at you, its eyes sad and lonely. It nods, more to itself than you, and then it joins you.
[/STORY]
[OUT]Welcome them.
[/NODE]

+[NODE]31
[STORY]
The creature stops and stares at you, its eyes sad and lonely. It nods, more to itself than you. It does not attack, but neither does it join you. It only growls:

'You slaughtered them all, and now you talk of love? I am a better being than this. I leave you to your fate, monster.'

It walks off into the shadows.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]32
[STORY]
The creature stops and stares at you, its eyes sad and lonely. It nods, more to itself than you. It does not attack, but neither does it join you. It only growls:

'You slaughtered them all, and now you talk of love? I am a better being than this. I leave you to your fate, monster.'

It walks off into the shadows, and you feel a heavy curse upon you.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]34
[STORY]
'Surrender! Surrender, like I did as a child. I should leave you to the bies to devour your flesh, just like you left me.

Instead, I curse you and all your kin!'

The creature disappears, but a curse lingers upon your souls. 
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Goblin Killer(2)
+[NODE]2
[STORY]
A group of goblins runs in your direction. They look scared, and it seems they are mostly elders or young ones.

Still, a few are armed.
[/STORY]
[OUT]Tell them to stop and state what they want.
[OUT]Attack!
[/NODE]

+[NODE]4
[STORY]
They comply, but they keep looking behind them nervously. One of them speaks:

'You, you will not kill us, right? We only want to go, we find a new place, we swear, just let us go!'
[/STORY]
[OUT]Ask what he is talking about.
[OUT]Shrug and let them pass.
[OUT]Ask for the short version of what is going on. [Skip story]
[/NODE]

+[NODE]5
[STORY]
'Oh, yes. Sorry. You just look a little like them. I think it's an elf, but may be a thin human...'
[/STORY]
[OUT]Who?
[/NODE]

+[NODE]7
[STORY]
'Goblin killer man! I kid you not, it isn't a mere name. That dude, or lady, they come and they kill and kill and kill. 

They murdered our village whole and then went for the little ones, spouting that they will only grow to become like any of us.'
[/STORY]
[OUT]Do they know why this goblin killer attacked them?
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[OUT]Tell them it seems dangerous, so they'd better keep running and leave.
[/NODE]

+[NODE]8
[STORY]
'Oh, yes, thank you. But maybe you help us? Maybe kill this psycho? Think of our children!'
[/STORY]
[OUT]Agree to help them stop this threat. Perhaps the goblin killer will listen to reason -- if not, then the blade.
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[OUT]No, leave.
[/NODE]

+[NODE]9
[STORY]
'Well, it is really weird, they keep saying we animals, we destroyed their farm and raped all the women. I mean, sure, we like a bit of big-folk lovin' but we not raid and rape! They must take us for orcs or something.'
[/STORY]
[OUT]Are orcs known for raping after a raid?
[OUT]So this goblin killer is hunting them now?
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[/NODE]

+[NODE]10
[STORY]
'Humans, orcs, sure. Dwarves and elves maybe less, and listen, I'm not sayin' it never happens with our lot. We have bad apples, just like you do. But we not do it all the time, I swear!'
[/STORY]
[OUT]So this goblin killer is hunting them now?
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[/NODE]

+[NODE]11
[STORY]
'Oh, yes. Sorry. You don't even look like them at all. I think it's an elf, but may be a thin human?.?.?.'
[/STORY]
[OUT]Who?
[/NODE]

+[NODE]12
[STORY]
'You, you will? Thanking you for sure! Yes, we pay, we pay what we can. 

A bit now, a bit when you bring their head to us. Oh, be careful, they come with others sometimes.'
[/STORY]
[OUT]Nod and leave to find this goblin killer.
[/NODE]

+[NODE]13
[STORY]
'Yes, yes, and he comes with friends often, they very bad. I do not know who harmed their family, but it wasn't my kin, I swear!'
[/STORY]
[OUT][Perception, intelligence, goblin or luck]Try to determine if he is telling the truth.
[OUT]Agree to help them stop this threat. Perhaps the goblin killer will listen to reason -- if not, then the blade.
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[/NODE]

+[NODE]15
[STORY]
You see the goblin sweating and averting eye contact with you. Others look equally uncomfortable. He is lying for sure.
[/STORY]
[OUT]Confront him and say he has one chance to tell you the truth!
[OUT]Attack the lying scum!
[/NODE]

+[NODE]16
[STORY]
'Oh, fine...so it was our boys that did it. We think. I mean, our village was doing bad, crops died, spirits did not help. 

Those boys, they say they go find us stuff. And they did. We did not ask where. But we did know. Maybe not all of it, but how else would they get so much? The raping, though, we really didn't know that. Those boys, they dead now, proper dead. We did not go, we did not kill. Our children, they good, please...'
[/STORY]
[OUT]Tell them you will try to speak to this goblin killer, but you cannot promise anything, since they are guilty in part.
[OUT]Agree that the whole village should not pay for the actions of some, especially children. Say that you will speak to this goblin killer.
[OUT]Say that whatever comes their way is well deserved. Leave.
[OUT]Tell them you will take their children to safety, but the rest should stand and pay the price for their crimes.
[OUT]They are guilty. Attack!
[/NODE]

+[NODE]17
[STORY]
'Take our children? Never! We will fight and we may die, but we will do it together. If you see us as guilty, then at least let us be. We will try to run.'
[/STORY]
[OUT]Let them run.
[OUT]Attack!
[/NODE]

+[NODE]20
[STORY]
'You, you will? Thanking you for sure! Oh, be careful, they come with others sometimes.'
[/STORY]
[OUT]Nod and leave to find this goblin killer.
[/NODE]

+[NODE]21
[STORY]
'You, you will? Thanking you for sure! Oh, be careful, they come with others sometimes.'
[/STORY]
[OUT]Nod and leave to find this goblin killer.
[/NODE]

+[NODE]22
[STORY]
The goblins depart as quickly as they can. Some look at you with gratitude for sparing their lives, some with fear, and others with anger and pain. One of the children waves goodbye and throws a flower as a gift.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]24
[STORY]
The goblins seem nervous. Perhaps they aren't telling you everything.
[/STORY]
[OUT]Ask them again for the truth.
[/NODE]

+[NODE]25
[STORY]
'Oh, yes. Well, we did know the goblins who may have done wrong to that killer. Perhaps that is why he targets us. But he is a monster, we did nothing to him!'
[/STORY]
[OUT]Say that it all seems fishy and they should deal with it themselves. Leave.
[OUT]Agree to help them stop this threat. Perhaps the goblin killer will listen to reason -- if not, then the blade.
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[/NODE]

+[NODE]27
[STORY]
'A goblin killer destroyed our village and is hunting us. But maybe you help us? Maybe kill this psycho? Think of our children!'
[/STORY]
[OUT]Agree to help them stop this threat. Perhaps the goblin killer will listen to reason -- if not, then the blade.
[OUT]Say that this seems like none of your business, but if they pay, you could deal with the killer.
[OUT]No, leave.
[OUT][Perception, intelligence, goblin or luck]Try to determine if he is telling the truth.
[/NODE]

+[NODE]28
[STORY]
You kill the goblins and gather the loot.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]30
[STORY]
'Oi, you same as that psychopath. You go die, we run.'

The goblins rush off and leave you on the ground.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Goblin Killer Spwn(3)
+[NODE]2
[STORY]
You come across an armed group of fighters. There is a human wearing a helmet, a tall, hooded orc with a bone wand, a zerca, an elf, and a dwarf.
The human has goblin heads on his belt, so this must be the crazed goblin killer you were told about.

They look you up and down but do not speak. 
[/STORY]
[OUT]Ask them what they want.
[OUT]Attack!
[OUT]Get off their path and leave.
[/NODE]

+[NODE]3
[STORY]
You come across an armed group of fighters. There is a human wearing a helmet, a tall, hooded orc with a bone wand, a zerca, an elf, and a dwarf.
The human has goblin heads on his belt, so this must be the crazed goblin killer you were told about.

They look at you and spit: 'Goblin scum, come to us? Good.'

They ready themselves for an attack.
[/STORY]
[OUT]Fight!
[OUT]Stand between them and your goblin friend and tell them to calm themselves -- there is no need for violence here. 
[OUT]Run away.
[/NODE]

+[NODE]5
[STORY]
You kill the notorious goblin slayer, and for a time, you receive letters of thanks from various goblin survivors. Some even send gifts.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]6
[STORY]
You are defeated, and any greenskins in your party do not make it out alive.
[/STORY]
[OUT]Run away.
[/NODE]

+[NODE]9
[STORY]
'Speak quickly, goblin lover. Did you see a band of the green scum running this way?'
[/STORY]
[OUT]Deny that you saw anything. 
[OUT]Say that you did see them, but you did not want to get involved, so you let them run.
[OUT]Say that you did see them and you killed them.
[/NODE]

+[NODE]11
[STORY]
'Fine, we will leave you be. But you are making a mistake with the goblin. The greenskins will always trick you. No better than vermin.'
[/STORY]
[OUT][Rat]!!!
[OUT]Nod and leave.
[OUT]Ask why they hate goblins so. You've never known them to be any more or less violent than humans.
[/NODE]

+[NODE]12
[STORY]
The warrior does not answer, only looks away, searching for goblin tracks. The priest speaks instead:

'Bah, you clearly have very little sense when it comes to friends. No matter. I need to catch the ones that got away. Begone from our path.'
[/STORY]
[OUT]Nod and leave.
[OUT]Ask why they hate goblins so. You've never known them to be any more or less violent than humans.
[/NODE]

+[NODE]15
[STORY]
The man in the helmet does not answer, but his elven companion speaks:

'As a mere child, our friend witnessed their home burned and pillaged, and a sister raped. Goblins were the culprits. Ever since, they have become an enemy. We trust our friend, so we give aid. Perhaps there is truth in their words. Goblins pretend to be weak and nice, but they always plot, scheme, meddle with evil spirits, and seek to spread darkness.'
[/STORY]
[OUT]Say that this is none of your concern and depart. 
[OUT][Hunter]Offer to tell them which way the goblins went.
[OUT]Tell them you understand grief, but murdering every goblin they meet seems extreme.
[/NODE]

+[NODE]16
[STORY]
The band does not listen. One of them shouts:

'Goblins must die!'

They begin to move, trying to circle you.
[/STORY]
[OUT]Attack!
[OUT]Run away, accepting that you will likely be hit by the elven archer as you escape.
[/NODE]

+[NODE]18
[STORY]
'Speak quickly. Did you see a band of the green scum running this way?'
[/STORY]
[OUT]Deny that you saw anything. 
[OUT]Say that you did see them, but you did not want to get involved, so you let them run.
[OUT]Say that you did see them and you killed them.
[/NODE]

+[NODE]23
[STORY]
'Oh. Disappointing for me, but dead is dead. Good.'

One of the others continues:

'The little shits raped and killed their dear sister. Been tracking them for years. Sneaky buggers. None deserves to live. That tribe, they worship some dark deity, want darkness to come back so humans lose again.'

The warrior in the helmet spits and the others nod solemnly.
[/STORY]
[OUT]Shrug and say it is none of your business, but the goblins are dead. Any reward?
[OUT]Nod and say in that case you did a good deed, right?
[OUT]Tell them you understand grief, but murdering every goblin they meet seems extreme. Then again, you did it too, so who are you to judge?
[/NODE]

+[NODE]24
[STORY]
The warrior nods and hands you their weapon:

'Here.'

The priest lets out a sigh and speaks up: 'You're not getting more words out of that one. We thank you. Your deed was good. Be well, and stay wary of the greenskins.'
[/STORY]
[OUT]Nod and leave.
[/NODE]

+[NODE]27
[STORY]
The warrior speaks:

'Yes, the children grow, hate, become adult goblins, and seek revenge. If I kill, I must kill all.'

The priest looks uncomfortable but remains silent.
[/STORY]
[OUT]Nod and leave.
[OUT]Shrug and say it is none of your business, but the goblins are dead. Any reward?
[/NODE]

+[NODE]28
[STORY]
The warrior nods and hands you their weapon:

'Yes.' He then hands you a cage with an animal inside. 'For killing more greenskins.'

The priest lets out a sigh and speaks up: 'You're not getting more words out of that one. We thank you. Your deed was good. Be well, and stay wary of the greenskins.'
[/STORY]
[OUT]Nod and leave.
[/NODE]

+[NODE]29
[STORY]
The warrior nods and speaks:

'That was stupid. They will run, grow. More killing.'

An uncomfortable silence falls over the group, and one of the companions speaks up:

'Better just leave now, goblin lover.'
[/STORY]
[OUT]Move away from them.
[OUT]Ask why they hate goblins so. You've never known them to be any more or less violent than humans.
[/NODE]

+[NODE]31
[STORY]
The warrior nods and speaks:

'That was stupid. They will run, grow. More killing.'

An uncomfortable silence falls over the group, and one of the companions speaks up:

'Better just leave now, goblin lover.'
[/STORY]
[OUT]Affirm that it was none of your concern, and you also did not aid the goblins -- merely let them go.
[OUT][Hunter]Offer to tell them which way the goblins went.
[OUT]Ask why they hate goblins so. You've never known them to be any more or less violent than humans.
[/NODE]

+[NODE]32
[STORY]
'You lie. You walk with them and lie for them. Foolish.'

An uncomfortable silence falls over the group, and one of the companions speaks up:

'Better just leave now, goblin lover.'

They depart and you realise later that one of their mages has cursed you.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]36
[STORY]
The warrior looks at you for a time, then nods again:

'Yes. Foolish, but perhaps better. They could have harmed you. We will leave now. Do not trust them.'

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]37
[STORY]
'Yes, good. Here.'

The warrior hands you payment for the information, and the band moves out in a hunting formation.
[/STORY]
[OUT]Wish them luck and leave.
[/NODE]

+[NODE]38
[STORY]
They watch you move, then depart also, and you realise later that one of their mages has cursed you.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]39
[STORY]
The warrior speaks:

'The children grow, hate, become adult goblins, and seek revenge. If I kill, I must kill all.'

The priest looks uncomfortable but remains silent. The others nod.
[/STORY]
[OUT][Hunter]Offer to tell them which way the goblins went.
[OUT][Turmoil]Agree with his point of view. An enemy left alive is an enemy forever in the shadows. Wish them good hunting.
[OUT][Harmony]Tell them that their hateful path will lead only to more death. If those goblins are indeed in league with darkness, they need to be stopped. But killing young ones in pre-emptive action will never be good.
[OUT]Say that this is none of your concern and depart. 
[/NODE]

+[NODE]40
[STORY]
They do not reply, but you see one of the mages about to cast a spell your way, before stopping and putting their wand away. 

The band moves out.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]41
[STORY]
The priest breaks down in tears, mumbling 'I told you so,' and 'damnation.'

The warrior looks very uncomfortable and turns to you:

'Now you've done it. You talk like this, but when they bring darkness, your gods will ask us to do something. 

But I will spare the green children, for now. Happy?'

The question was clearly more for the priest than for you. The zerca nods and the band moves away.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Goblin Killer Spwn2(4)
+[NODE]2
[STORY]
You come across an armed group of fighters. There is a human wearing a helmet, a tall, hooded orc with a bone wand, a zerca, an elf, and a dwarf.
The human has goblin heads on his belt, so this must be the crazed goblin killer you were told about.

They look you up and down but do not speak. 
[/STORY]
[OUT]Ask if they are the ones chasing a group of goblins.
[OUT]Attack!
[OUT]Get off their path and leave.
[OUT][Goblin]Murdering scum. Attack them!
[/NODE]

+[NODE]5
[STORY]
You kill the notorious goblin slayer, and for a time, you receive letters of thanks from various goblin survivors. Some even send gifts.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]6
[STORY]
You are defeated, and any greenskins in your party do not make it out alive.
[/STORY]
[OUT]Run away.
[/NODE]

+[NODE]15
[STORY]
The man in the helmet does not answer, but his elven companion speaks:

'As a mere child, our friend witnessed their home burned and pillaged, and a sister raped. Goblins were the culprits. Ever since, they have become an enemy. We trust our friend, so we give aid. Perhaps there is truth in their words. Goblins pretend to be weak and nice, but they always plot, scheme, meddle with evil spirits, and seek to spread darkness.'
[/STORY]
[OUT]Say that you know what the goblins did to their family, but surely the whole village does not need to pay for it, at least not with death?
[OUT]The goblins did not tell you this. Offer to reveal which way they went.
[OUT]Try to convince them to let this go. Surely children can be spared?
[OUT]Attack!
[OUT][Harmony]Tell them that their hateful path will lead only to more death. If those goblins are indeed in league with darkness, they need to be stopped. But killing young ones in pre-emptive action will never be good.
[OUT][Turmoil]Agree with his point of view. An enemy left alive is an enemy forever in the shadows. Wish them good hunting and point them to where the goblins went.
[/NODE]

+[NODE]16
[STORY]
The band does not listen. One of them shouts:

'Goblins must die!'

They begin to move, trying to circle you.
[/STORY]
[OUT]Attack!
[OUT]Run away, accepting that you will likely be hit by the elven archer as you escape.
[/NODE]

+[NODE]18
[STORY]
'Yes. You have seen them? Where?'
[/STORY]
[OUT][Intelligence]Tell them you killed the goblins.
[OUT]Ask them why they are chasing mere children and elders. Surely destroying their village was enough.
[OUT]Try to convince them to let this go. Surely children can be spared?
[/NODE]

+[NODE]23
[STORY]
'Oh. Disappointing for me, but dead is dead. Good.'

One of the others continues:

'The little shits raped and killed their dear sister. Been tracking them for years. Sneaky buggers. None deserves to live. That tribe, they worship some dark deity, want darkness to come back so humans lose again.'

The warrior in the helmet spits and the others nod solemnly.
[/STORY]
[OUT]Shrug and say it is none of your business, but the goblins are dead. Any reward?
[/NODE]

+[NODE]24
[STORY]
The warrior nods and hands you their weapon:

'Here.'

The priest lets out a sigh and speaks up: 'You're not getting more words out of that one. We thank you. Your deed was good. Be well, and stay wary of the greenskins.'
[/STORY]
[OUT]Nod and leave.
[/NODE]

+[NODE]28
[STORY]
The warrior nods and grunts:

'Yes.' He then hands you a cage with an animal inside. 'For killing more greenskins.'

The priest lets out a sigh and speaks up:

 'You're not getting more words out of that one. We thank you. Your deed was good. Be well, and stay wary of the greenskins.'
[/STORY]
[OUT]Nod and leave.
[/NODE]

+[NODE]31
[STORY]
The warrior speaks:

'The children grow, hate, become adult goblins, and seek revenge. If I kill, I must kill all.'

The priest looks uncomfortable but remains silent. The others nod.
[/STORY]
[OUT]Ask why they hate goblins so. You've never known them to be any more or less violent than humans.
[OUT]Say that you know what the goblins did to their family, but surely the whole village does not need to pay for it, at least not with death?
[OUT]Try to convince them to let this go. Surely children can be spared?
[OUT]Attack!
[/NODE]

+[NODE]32
[STORY]
'Yes. This once, I will leave them be. But if they grow to kill and bring darkness, you will bear the blame also.'

They move out.
[/STORY]
[OUT]Say that it is the right thing to do. The cycle of violence should end. Wish them well and leave.
[/NODE]

+[NODE]37
[STORY]
'Yes, good. Here.'

The warrior hands you payment for the information, and the band moves out in a hunting formation.
[/STORY]
[OUT]Wish them luck and leave.
[/NODE]

+[NODE]40
[STORY]
'Yes, you understand. Here.'

The warrior hands you payment for the information, and the band moves out in a hunting formation.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]41
[STORY]
The priest breaks down in tears, mumbling 'I told you so,' and 'damnation.'

The warrior looks very uncomfortable and turns to you:

'Now you've done it. You talk like this, but when they bring darkness, your gods will ask us to do something. 

But I will spare the green children, for now. Happy?'

The question was clearly more for the priest than for you. The zerca nods and the band moves away.
[/STORY]
[OUT]Say that it is the only right thing to do for harmony.
[/NODE]

+[NODE]44
[STORY]
'So it was them, good. I was not certain. Now, did you see where they went? They will pay.'
[/STORY]
[OUT]Try to convince them to let this go. Surely children can be spared?
[OUT]Yes, tell them where the goblins went.
[OUT]Attack!
[/NODE]

+[NODE]54
[STORY]
The goblins find you later and thank you for giving them time to run. They found some kin and together they leave you a reward.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --VOLCANIC(5)
+[NODE]2
[STORY]
You arrive in the volcanic lands, so named for the many active volcanoes and lava pits. 

From the ash-covered plains and burned-out drylands to the high peaks of the volcanic mountains, the ever-hot, stifling air and constant threat of eruption keeps the wildlife and any mad settlers on their toes.

Beware, these lands are known to be more challenging than your homeland.
[/STORY]
[OUT]Explore.
[/NODE]

+[NODE]3
[STORY]
This harsh, unforgiving land is home to the alpha clans, who enjoy the daily challenge of the environment. 

You can also find lava trolls and even the occasional dragon lurking amongst the molten rocks.

This land is rich in obsidian and other rocks, and beetle jelly is the local specialty.
[/STORY]
[OUT]Move out.
[/NODE]

[/EVENT]

-- [EVENT] --DARKNESS(6)
+[NODE]2
[STORY]
You have arrived in the lands of darkness. Here the last remnants of the deadly black mist remain ever-lingering and the sun is constantly dimmed, creating a feeling of perpetual dusk.

Beware of night terrors and creatures of the dark skulking in every corner.

Beware, these lands are known to be more challenging than your homeland.
[/STORY]
[OUT]Explore.
[/NODE]

+[NODE]3
[STORY]
Despite its dangers, the land of darkness is home to the spirit talkers, goblin tribes that feel at ease in the dark and take advantage of the thinned veil between this world and the greyworld of the spirits.

The shadowkin, a more elusive and dangerous faction of those changed by darkness long ago, lurks in the shadows.

The island is known for dragon bones and mushrooms.
[/STORY]
[OUT]Move out.
[/NODE]

[/EVENT]

-- [EVENT] --ICY(7)
+[NODE]2
[STORY]
You have arrived in the ice lands, the forever-frozen pastures ruled by the mysterious dziad mroz. 

When the cold swallowed this island, cities fell into instant ruin and folk fled, unable to live in such a harsh environment.

Beware, these lands are known to be more challenging than your homeland.
[/STORY]
[OUT]Explore.
[/NODE]

+[NODE]3
[STORY]
You will not find many creatures alive here, but the ice demons are known to make lairs from time to time. 

Dziad mroz is a creature who is many but one. You can also find the terrifying ice trolls, whimsical sniezynka, or mischievous icicles roaming the ice plains or hiding amongst the mountain peaks. 

You may find diamonds and nuts here.

[/STORY]
[OUT]Move out.
[/NODE]

[/EVENT]

-- [EVENT] --ANCFOREST(8)
+[NODE]2
[STORY]
You have arrived on the fabled ancient forest island. It is said that this land emerged from hiding when the earth shattered, much like the cities of the dwarves. 

Much of the woodland here has been destroyed, but what remains is no less breathtaking.

Beware, these lands are known to be more challenging than your homeland.
[/STORY]
[OUT]Explore.
[/NODE]

+[NODE]3
[STORY]
These lands are home to the elder races. Both woodlandkin and forest demons stand as guardians of the evergreen woodlands, hoping to one day regrow what was lost.

Creatures of the forest are fiercely territorial, so one should remain on guard. 

Ancient wood and exotic spices can be found here.
[/STORY]
[OUT]Move out.
[/NODE]

[/EVENT]

-- [EVENT] --METALLIC(9)
+[NODE]2
[STORY]
You have arrived in the metallic lands, an unforgiving region of stone and metal. When the Shattering unearthed the ancient dwarven cities, the land around them changed. Even trees became hollow husks of iron, and grass fossilised into rock. 

Beware, these lands are known to be more challenging than your homeland.
[/STORY]
[OUT]Explore.
[/NODE]

+[NODE]3
[STORY]
This foreboding land is the home to the earthbound, who were forced out of their underground kingdoms. The earthbound are a proud people, protective of their privacy. 

The land of metal and stone is also home to rock trolls and dragons, so it is not for the faint of heart. 

You can gather mithril and eggs here.
[/STORY]
[OUT]Move out.
[/NODE]

[/EVENT]

-- [EVENT] --Chosen death(10)
+[NODE]2
[STORY]
You allowed your chosen to die! Without the divine bond, you will no longer have any power or influence over the world, and thus your reign will end.

You have one more chance to remain in the game. Call upon the spirit of the cosmic tree itself and ask for the soul of your chosen to be returned to you. 

Remember, the price is high and it will get higher every time you abuse your divine power like this. 
[/STORY]
[OUT]Pay with two lives to return the one.
[OUT]Pay with four lives to return the one.
[OUT]Pay with six lives to return the one.
[OUT]Pay with eight lives to return the one.
[OUT]Pay with ten lives to return the one.
[OUT]Pay with fifteen lives to return the one.
[OUT]Pay with twenty lives to return the one.
[OUT]You cannot or will not pay. It is time to end this journey.
[/NODE]

+[NODE]4
[STORY]
You paid your bloody dues and the spirit of your chosen is returned. Now pray that it comes back in the right flesh?.?.?.
[/STORY]
[OUT]Await your chosen.
[/NODE]

[/EVENT]

