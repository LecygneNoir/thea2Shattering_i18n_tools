-- [EVENT] --StingersTradesmall(0)
+[NODE]2
[STORY]
The Stingers show you their stash to trade.
[/STORY]
[OUT]Trade.
[OUT]Trade.
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]7
[STORY]
'You no friends of ours you pay us extra first.'
[/STORY]
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]11
[STORY]
'Good trading with you. Now buzz off.'
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --StingersTradeadv(1)
+[NODE]2
[STORY]
The Stingers show you their stash to trade.
[/STORY]
[OUT]Trade.
[OUT]Trade.
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]7
[STORY]
'You are no friends of ours so you'll have to pay us extra first.'
[/STORY]
[OUT]Trade.
[OUT]Come back another time.
[/NODE]

+[NODE]11
[STORY]
'Good trading with you. Now buzz off.'
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Army Stingers(2)
+[NODE]2
[STORY]
You hear a dangerous buzz of the Stinger patrol following you!
[/STORY]
[OUT]Greet your allies. 
[OUT]Stay cautious, but speak to them.
[OUT]Try to talk your way out of this.
[OUT]Fight!
[OUT]Move away.
[OUT]Bribe the Stingers to show you are a friend.
[/NODE]

+[NODE]3
[STORY]
'You, you good, we don't fight.'
[/STORY]
[OUT]Ask if they want to trade.
[OUT]Exchange food and show respect.
[/NODE]

+[NODE]11
[STORY]
They do not answer, they keep coming towards you.
[/STORY]
[OUT]Leave.
[OUT]Try to talk your way out of this.
[OUT]Fight!
[/NODE]

+[NODE]17
[STORY]
You defeat the Stingers.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]19
[STORY]
The Stingers defeat you and infect you with a toxin before you manage to escape.
[/STORY]
[OUT]Run.
[/NODE]

+[NODE]25
[STORY]
The Stingers move away and you even gain some respect from them.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]30
[STORY]
The Stingers do not listen, in fact they use your distracted state to bite you and infect you with a toxin before you manage to escape.
[/STORY]
[OUT]Run.
[/NODE]

[/EVENT]

-- [EVENT] --Stingers Lair_(3)
+[NODE]3
[STORY]
You find a buzzing hub of animal activity. It looks a bit like a large beehive, but then you also spot some underground pairs and various other nests that are organised into one settlement.

Several bees approach you, and speak:
'You, you no beast. You want fight or you want trade?'

[/STORY]
[OUT]Try to speak to them.
[OUT]Attack!
[/NODE]

+[NODE]4
[STORY]
The lair is buzzing with life, but they try to stay clear of you.

You can trade or heal toxins, and some spiders may wish to join you for a fee.

When you're friends, you can recruit some bees, or spider queens, or train your beasts to become stronger, healthier or wiser.
[/STORY]
[OUT]Trade.
[OUT]Leave.
[OUT]Attack!
[OUT][Beast]Go see about the beast training.
[OUT]Something it tugging at you. Look down.
[OUT]Pay with food to have toxins removed by the snakes.
[OUT]Pay for a rare pet spider [Every 80 turns].
[/NODE]

+[NODE]7
[STORY]
You find a buzzing hub of animal activity. It looks a bit like a large beehive, but then you also spot some underground pairs and various other nests that are organised into one settlement.

But you are no friends to the stingers, so they attack!

[/STORY]
[OUT]Defend yourself!
[OUT]Run!
[/NODE]

+[NODE]8
[STORY]
The stingers' lair is destroyed. Its inhabitants are dead.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]11
[STORY]
The poisonous bite of the stingers proves too strong and you are forced to flee.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]16
[STORY]
A queen bee examines you:

'Darkness came long ago and changed us. We know how to master our bodies and minds. 

Pay us with meat, lots of it! And we shall train you too, beast friend.'


[/STORY]
[OUT]Train the beasts 
[OUT]Politely decline.
[/NODE]

+[NODE]22
[STORY]
A queen bee examines you:

'Darkness came long ago and changed us. We know how to master our bodies and minds. 

Pay us with meat, lots of it! And we shall train you too, beast friend.'


[/STORY]
[OUT]Yes, accept.
[OUT]Politely decline.
[/NODE]

+[NODE]26
[STORY]
A small critter likes you and wants to join, but you need to pay the stingers.
[/STORY]
[OUT]Take it in.
[OUT]No, go back to town.
[/NODE]

+[NODE]33
[STORY]
A small critter likes you and wants to join, but you need to pay the stingers.
[/STORY]
[OUT]Take it in.
[OUT]No, go back to town.
[/NODE]

[/EVENT]

