-- [EVENT] --EncounterRacesDiff2(0)
+[NODE]2
[STORY]
Someone approaches you.
[/STORY]
[OUT]Stay alert.
[OUT]Stay alert.
[/NODE]

[/EVENT]

-- [EVENT] --Goblin travelers(1)
+[NODE]2
[STORY]
As you're about to set up camp, a group of goblin travellers approaches, asking for shelter. 
[/STORY]
[OUT][Goblin]Greet your kin and welcome them to your camp.
[OUT][Orc]Intimidate them into giving you their stuff or else you'll attack.
[OUT]Welcome them to your camp. You should be helping one another.
[OUT]It is the way of harmony to help. Invite them and share food.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Attack!
[OUT]Refuse. You will not share bread with strangers. 
[OUT]Refuse. You will not share bread with strangers. 
[OUT]Refuse. You will not share bread with strangers. 
[/NODE]

+[NODE]3
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]6
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

There is a chance the spirits imparted wisdom to you as well. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]8
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

And then you realise you drifted off while they robbed you and ran off!
[/STORY]
[OUT]Catch them and kill them!
[OUT]Damned thieves. Leave.
[/NODE]

+[NODE]10
[STORY]
You kill the goblins and loot their bodies.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]11
[STORY]
The goblins beat you and take some of your things, but they leave you alive, at least. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

There is a chance the spirits imparted wisdom to you as well. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]24
[STORY]
It is wise to stay cautious. You feel your domain strengthen. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]26
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

They like your company so much, they give you a stash of the weed and one of their puppies before they leave. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]28
[STORY]
'Oi, oi, easy there, fellas, easy. Here, have some of our stuff. And we, just, we go over there, yeah?'

As the goblins walk off, you hear a lot of swearing.
[/STORY]
[OUT]Take their stuff and continue setting up camp.
[/NODE]

+[NODE]31
[STORY]
'Oh, yes, here we go. You big, you strong, you beat up the gobbos, make you feel better, eh? Stupid oafs. 

We leave you be!'

They walk off. 
[/STORY]
[OUT]Let them go.
[OUT]Attack!
[/NODE]

+[NODE]34
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

You realise, however, that the stuff they shared was pretty strong, and you all feel very ill. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]35
[STORY]
You refuse and they walk off, but in the morning,  you realise they must have sneaked back and stolen some of your stuff!
[/STORY]
[OUT]Catch them and kill them!
[OUT]Damned thieves. Leave.
[/NODE]

[/EVENT]

-- [EVENT] --Old orcs(2)
+[NODE]2
[STORY]
A group of elderly looking orcs approaches your camp. They show their hands in a gesture of peace, but they do still have weapons by their side. 
[/STORY]
[OUT]Wait until they approach to see what they want.
[OUT]Stand firm and show them your weapons so they go on their way.
[OUT]Stand firm and show them your weapons so they go on their way.
[OUT]Stand firm and show them your weapons so they go on their way.
[OUT]Attack!
[/NODE]

+[NODE]3
[STORY]
'Good travels to you! May we join you and share meat and spirits?'
[/STORY]
[OUT][Orc]Nod, but ask them why a group of elders is not trying to die in the pits, but instead wandering the roads looking for friends.
[OUT]Agree.
[OUT]Refuse. It is never safe to trust strangers, especially not armed ones.
[OUT]Refuse. It is never safe to trust strangers, especially not armed ones.
[OUT]Attack!
[/NODE]

+[NODE]9
[STORY]
The group sits by the fire and passes along some dried meat and strong spirits:

'I am Broken Night Walker. It is an honour to drink and eat together. This way, we are not enemies. The rite of journey binds us. Would you youngsters like to hear some elder wisdom?'

[/STORY]
[OUT][Orc]Say that you think they will ask you now:  what is best in life?
[OUT][Elf]Assert that you are hardly a 'youngster' yourself.
[OUT]Agree, and ask if this means some kind of combat.
[OUT]Decline respectfully. 
[/NODE]

+[NODE]10
[STORY]
The orcs are dead.
[/STORY]
[OUT]Gather their loot and leave.
[/NODE]

+[NODE]12
[STORY]
The elderly orcs beat you badly and take some of your stuff, but they spare your lives. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]15
[STORY]
'Ha, if only we were younger, we could have ourselves a good old beating. But no, you have not offended our honour, nor we yours. 

We are elder, a rarity for our kind, I'll have you know. 

We simply ask you: what is best in life??
[/STORY]
[OUT]Warm soup, good teeth, and a strong bladder.
[OUT]Warm soup, good teeth, and a strong bladder.
[OUT]To go through life at peace with yourself.
[OUT]To go through life at peace with yourself.
[OUT]To crush your enemies, see them driven before you, and hear the lamentations of their men.
[OUT]To crush your enemies, see them driven before you, and hear the lamentations of their men.
[/NODE]

+[NODE]16
[STORY]
After resting awhile, the orcs move on, leaving you a small thank-you gift.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]17
[STORY]
'Yes, yes, a warm meal, teeth to eat it with, and a bladder that won't make you piss yourself -- that is what you treasure in old age.

And before you ask, it is not what most of our kind believes, but we are clanless, have been for decades. We found ourselves on a different path.'
[/STORY]
[OUT]Thank them for their wisdom.
[/NODE]

+[NODE]18
[STORY]
'Stuff your stomachs with food, shit under yourselves, and die in a feather bed, eh? Never!

Eat only the flesh of what you killed, fuck whomever you can, and respect a good enemy, for only they can give you honourable death!'

[/STORY]
[OUT]Agree to disagree.
[OUT]Argue your point.
[/NODE]

+[NODE]19
[STORY]
'True, in our old age, we seek to be at peace, to end the turmoil and rest.

And before you ask, it is not what most of our kind believes, but we are clanless, have been for decades. We found ourselves on a different path.'
[/STORY]
[OUT]Thank them for their wisdom.
[/NODE]

+[NODE]22
[STORY]
'It is what we once thought, believed, lived by. 

But is it true? And do you believe it, or did you say so to please our old ears?

Our lesson, then, is: if an old orc can say peace is not a lie, anything in life can change.'
[/STORY]
[OUT]Thank them for their wisdom.
[/NODE]

+[NODE]24
[STORY]
'Yes, yes! Ah, good, you already know the path to glory!

You do not need our wisdom, then, but you may need a good blade to practise what we preach!'

The orc hands you her weapon.
[/STORY]
[OUT]Thank them for their wisdom.
[/NODE]

+[NODE]27
[STORY]
'Good, I feared your time with the human gods may have tamed your spirits. So, answer me?'
[/STORY]
[OUT]Warm soup, good teeth, and a strong bladder.
[OUT]Warm soup, good teeth, and a strong bladder.
[OUT]To go through life at peace with yourself.
[OUT]To go through life at peace with yourself.
[OUT]To crush your enemies, see them driven before you, and hear the lamentations of their men.
[OUT]To crush your enemies, see them driven before you, and hear the lamentations of their men.
[/NODE]

+[NODE]30
[STORY]
Your clarity of mind sits well with the domain of intellect. 

You feel strengthened. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]33
[STORY]
You see a flash of fire in their eyes, but the one who leads them holds out her hand:

'We are on a journey and will find honour in death, but not here. That is all we will say.'
[/STORY]
[OUT]Nod again, and gesture for them to sit.
[OUT]Growl at their breaking of tradition.
[/NODE]

+[NODE]34
[STORY]
'You are not one to speak, follower of human deities.

We honour our ways. We are elder. You'll be wise to clench your teeth and listen, pup!'
[/STORY]
[OUT]Nod, and gesture for them to sit.
[OUT]Tell them it is time they earned their death honour. Attack!
[/NODE]

+[NODE]35
[STORY]
'Bah, you surprise me, young one. Somehow I feel it is we who are being tested here. 

You'd like me to say now, NO! That is wisdom for weaklings!

But a warm meal, teeth to eat it with, and a bladder that won't make you piss yourself -- that is what you treasure in old age.'
[/STORY]
[OUT]Question their honour and heritage. Ask where they come from.
[OUT]Agree. Say that you too have found a wiser path in life through the cosmic pantheon. Thank them and wish them well on their journey.
[/NODE]

+[NODE]36
[STORY]
'You mock us, mongrel pup! You'd be wise to respect age, for I have skinned and mounted many a youngster like yourself, and I am not done yet!'
[/STORY]
[OUT]Apologise. Give the correct answer: crush your enemies, see them driven before you, and hear the lamentations of their men!
[OUT]Assert that your answer is the right one, for you have changed your path. 
[/NODE]

+[NODE]37
[STORY]
'Ha, I suppose this was easy enough, but I did worry that your human pantheon had stifled the fire within you.

Yes, pup, and do you know the true meaning behind it yet?'
[/STORY]
[OUT]Stay forever in motion, embrace change, and spit on peace and stagnation.
[OUT]Say that the meaning is literal. 
[/NODE]

+[NODE]38
[STORY]
'Good, good. We are on our way to fight a final battle. 

Our young were slain. We go to avenge our clan and die in fire. But you, you go with our blessing and our wisdom. Tear your enemies apart and burn your friends with passion!'

The orcs leave, and later you find they left you a gift.
[/STORY]
[OUT]Bow in respect of the elders. 
[OUT]Bow in respect of the elders. 
[/NODE]

+[NODE]39
[STORY]
'Hmm, you have a ways to go yet, but your spirit is true.

Our young were slain. We go to avenge our clan and die in fire. But you, you go with our blessing and our wisdom. Tear your enemies apart and burn your friends with passion!'

The orcs leave, and later you find they left you a gift.
[/STORY]
[OUT]Bow in respect of the elders. 
[OUT]Bow in respect of the elders. 
[/NODE]

+[NODE]42
[STORY]
'You're young, and your fire burns bright. Yet you too walk a path apart from your clan, no? 

We are clanless, have been for many decades. And we had to learn that the old ways are not the only path. 

So this is our wisdom for you. Soothe your fires, extinguish them if you can, and perhaps peace will be your reward.'
[/STORY]
[OUT]Spit in their face and tell them to leave, now!
[OUT]Attack them for this insolence!
[OUT]Agree and thank them for the lesson. 
[/NODE]

+[NODE]44
[STORY]
The orcs only nod and move away. You see one of them is struggling not to hit you, but he follows his matriarch out.

Despite the abrupt ending, you feel you have learned a valuable lesson today: not all of your kin are sane.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]46
[STORY]
The elderly orcs have earned their death honour now!
[/STORY]
[OUT]Commit them to the funeral pyres. 
[/NODE]

+[NODE]48
[STORY]
'Young, insolent pups!

But you fought well. You have fire in your souls and it makes us proud, even though we walk another path. May peace be with you.'

The last words are spoken with much amusement, as the orcs know they sting you hard. The orcs walk off, but they take one weapon from you.
[/STORY]
[OUT]Leave in shame. 
[/NODE]

+[NODE]49
[STORY]
'Bah, you surprise me, young one. Somehow I feel it is we who are being tested here. 

You'd like me to say now, NO! That is wisdom for weaklings!

But peace is not a lie.'
[/STORY]
[OUT]Question their honour and heritage. Ask where they come from.
[OUT]Agree. Say that you too have found a wiser path in life through the cosmic pantheon. Thank them and wish them well on their journey.
[/NODE]

+[NODE]50
[STORY]
'Bah, you surprise me, young one. I thought following the human gods may have changed you for the better. 

The constant turmoil, the warring, the change -- these are not the only ways.'
[/STORY]
[OUT]Agree. Say that you too have found a wiser path in life through the cosmic pantheon. Thank them and wish them well on their journey.
[/NODE]

+[NODE]51
[STORY]
'Bah, it's a matter of seeing, isn't it? For your kind, you look young to me, with none of the dark taint from days of darkness. So, what, a few centuries at best? Baby by your standards, no?

I am messing with you. Now, you want to listen to old orc wisdoms, or no?'
[/STORY]
[OUT]Agree, and ask if this means some kind of combat.
[OUT]Decline respectfully. 
[/NODE]

+[NODE]52
[STORY]
'Ha, if only we were younger, we could have ourselves a good old beating. But no, you have not offended our honour, nor we yours. 

We are elder -- well, maybe not for you, but still -- a rarity for our kind, I'll have you know. We simply ask you: what is best in life??
[/STORY]
[OUT]To go through life at peace with yourself.
[OUT]To go through life at peace with yourself.
[OUT]To crush your enemies, see them driven before you, and hear the lamentations of their men.
[/NODE]

+[NODE]53
[STORY]
'Ha, somehow I guessed you long-ears would go for that. 

But what is peace if not stagnation, eh? If your harmony and light prevailed, as it seeks to do now, we would all be empty husks. 

Change, conflict, passion -- only if you accept them will you achieve your peace, my long-living friend.'

[/STORY]
[OUT]Bow in thanks for the lesson.
[OUT]Smile and assert that ones as crude and short-lived as they cannot seek to teach you anything true.
[OUT]Smile and assert that ones as crude and short-lived as they cannot seek to teach you anything true.
[/NODE]

+[NODE]54
[STORY]
The orc shrugs her shoulders and continues to tell orcish stories till they all go to sleep. 

You are surprised they did not take offence nor offer any verbal riposte. This act proved a more valuable lesson than their words. 
[/STORY]
[OUT]Leave.
[OUT]Attack them!
[/NODE]

+[NODE]55
[STORY]
'You fucking pointy-eared snobs never change! We try to be all civil-like, pass on life lessons, but all you can do is spit on us. So now we will show you what you expected, scum!'

They charge at you!
[/STORY]
[OUT]Fight!
[/NODE]

+[NODE]56
[STORY]
'Indeed, we have come to understand just that. 

By the faint twitch in your brow, elder one, I assume your surprise. As you walk the path of the human deities, so did we find that path. Balance is what we seek. 

But remember, creature of harmony, balance only happens when you also allow the other side in.'
[/STORY]
[OUT]Bow in thanks for the lesson.
[/NODE]

+[NODE]58
[STORY]
'Ha, I'd never guess you long-ears would go for that. 

But what is peace if not stagnation, eh? If your harmony and light prevailed, as it seeks to do now, we would all be empty husks. 

But change, conflict, passion -- only if you accept them will we achieve true glory!'

[/STORY]
[OUT]Bow in thanks for the lesson.
[OUT]Smile and assert that ones as crude and short-lived as they cannot seek to teach you anything true.
[OUT]Smile and assert that ones as crude and short-lived as they cannot seek to teach you anything true.
[/NODE]

+[NODE]59
[STORY]
'You are young and follow those smooth-talking gods of yours, but you have Zorya, you have Triglav, you have Horz, so you listen to them, pup! Do not let your hearts go soft or the enemy will destroy us all!'
[/STORY]
[OUT]Agree to disagree.
[OUT]Insist that warfare is not wisdom, only a means one must sometimes use.
[/NODE]

+[NODE]60
[STORY]
You see the old orc's eyes blaze as she clenches her fist, but she exhales and only adds:

'You asked for our wisdom, and we gave it. You are not our enemy. We seek our death honour elsewhere. We should rest now, for you never know what the light may bring in the morn.'
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]64
[STORY]
'Shame, shame on you, pup. You will die an honourless death; the mothers will not embrace you, and Zorya will never hunt by your side. Shame...'
[/STORY]
[OUT]Agree to disagree.
[OUT]Attack them!
[/NODE]

+[NODE]66
[STORY]
The orcs take your stance as a challenge and charge!
[/STORY]
[OUT]Defend yourself!
[/NODE]

+[NODE]67
[STORY]
'Peace. Sing songs and hold hands like the pinty ears, eh? Never!

Eat only the flesh of what you killed, fuck whomever you can, and respect a good enemy, for only they can give you honourable death!'

[/STORY]
[OUT]Agree to disagree.
[OUT]Argue your point.
[/NODE]

[/EVENT]

-- [EVENT] --Elves(3)
+[NODE]2
[STORY]
As you set up camp, a group of elves approach you carefully. Most of the group is carried on stretchers, ill.

'May we seek shelter at your side?'
[/STORY]
[OUT][Harmony]As the path of harmony dictates, ask them in.
[OUT]Invite them in.
[OUT]Tell the strangers they must camp elsewhere, as you will not endanger your own.
[OUT]Tell the strangers they must camp elsewhere, as you will not endanger your own.
[OUT]It is the strongest who will survive in this world. Attack!
[/NODE]

+[NODE]4
[STORY]
'I thank thee, friends. I feel you walk the path of harmony or light. This is good.

My brethren were assaulted by necromancy, and a curse now blights their light. We do not have a mage with us, thus we make haste to our home. Alas, I do not think they have that much time left.'
[/STORY]
[OUT][Elf mage]This curse is made of darkness. Use your elven light and mastery of magic to try to cleanse your brethren, even if it hurts you. 
[OUT][Striga masters]Say that you can smell the curse and you know it well. You can take it.
[OUT][Magic users]Since it is a curse, offer to try to break it with a cleansing ritual.
[OUT]Offer them a place to rest, for you cannot do anything more.
[OUT]Offer them a place to rest, for you cannot do anything more.
[OUT]Tell them they must leave, for you do not have wise ones and you do not want a curse.
[OUT]Tell them they must leave, for you do not have wise ones and you do not want a curse.
[/NODE]

+[NODE]6
[STORY]
The path of intellect or nature is not driven by sympathy. You feel strengthened by your resolve. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]8
[STORY]
You kill the elves and feel not only that turmoil has protected you from a curse these enemies carried, but also that you have been blessed by this swift act of conflict.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]9
[STORY]
You are defeated. The elves depart and do not seek to kill you, but a curse they carried rubbed off on you during combat.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]10
[STORY]
You kill the elves. Alas, their souls were inflicted with a dark curse, and some of it now clings to your spirits!

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]11
[STORY]
You kill the elves.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]13
[STORY]
Despite your best efforts, the curse is too strong. Several of the elves perish, their bodies turning to silver dust, and you sing the song of life to speed them on their journey to light.

Worse yet, a weaker remnant of the curse now ails you. Your surviving kin depart in a sombre mood, but they leave you a parting gift as thanks. 

[/STORY]
[OUT]Bid them farewell. 
[/NODE]

+[NODE]14
[STORY]
The dark curse breaks under your assault, and your brethren soon recover their vitality. 

Hastened by duty, they depart, but they leave you with a blessing and gifts.

[/STORY]
[OUT]Bid them farewell. 
[OUT]Bid them farewell. 
[/NODE]

+[NODE]16
[STORY]
The elves bow in thanks and tend to their sick. Alas, none of the afflicted lasts the night, and a remnant of the curse lingers on you!

The elves apologise and depart in grief, leaving only some supplies behind.

[/STORY]
[OUT]Leave.
[OUT]Attack them!
[/NODE]

+[NODE]17
[STORY]
The elves bow in thanks and tend to their sick. Alas, none of the afflicted lasts the night. Their bodies turn to silver dust, and their kin sing a beautifully sorrowful song that fills your hearts with hope.

The elves apologise and depart in grief, leaving some supplies and a gift.

[/STORY]
[OUT]Among the gifts, you discover a pet. Take it in.
[OUT]Leave.
[/NODE]

+[NODE]21
[STORY]
You take the burning coals from the fire. To cleanse the soul with light, you cover the sick with oak leaves and dirt. To ask mother earth her blessing, you mix your saliva with three fingernails of an elder and the blood of an animal, and you drink it.

The elves look at you with an air of confusion and disgust, but when their brethren rise, cleansed and healed, they bow to you and leave you gifts in thanks.

[/STORY]
[OUT]Bid them farewell. 
[OUT]Bid them farewell. 
[/NODE]

+[NODE]26
[STORY]
The elves look at you with distrust, but nod.

You sit by the cursed elfkin and let out your leech tongue into their necks. By taking their blood, you also take the curse.

The blood hurts you, but you have cured them.
[/STORY]
[OUT]Demand payment.
[OUT]Tell the elves their kin will be well now. 
[/NODE]

+[NODE]28
[STORY]
The elf who asked your aid scowls at you, but nods.

'You have taken the darkness upon yourself, despite the hurt our blood caused you. This shall be remembered and rewarded.'

They place a pack at your feet.
[/STORY]
[OUT]Bid them farewell. 
[/NODE]

+[NODE]29
[STORY]
'You have taken the darkness upon yourself, despite the hurt our blood caused you. This shall be remembered and rewarded.'

They place a pack at your feet.
[/STORY]
[OUT]Bid them farewell. 
[/NODE]

+[NODE]30
[STORY]
'I thank thee, friends. 

My brethren were assaulted by necromancy, and a curse now blights their light. We do not have a mage with us, thus we make haste to our home. Alas, I do not think they have that much time left.'

[/STORY]
[OUT][Elf mage]This cure is made of darkness. Use your elven light and mastery of magic to try to cleanse your brethren, even if it hurts you. 
[OUT][Striga masters]Say that you can smell the curse and you know it well. You can take it.
[OUT][Magic users]Since it is a curse, offer to try to break it with a cleansing ritual.
[OUT]Offer them a place to rest, for you cannot do anything more.
[OUT]Offer them a place to rest, for you cannot do anything more.
[OUT]Tell them they must leave, for you do not have wise ones and you do not want a curse.
[OUT]Tell them they must leave, for you do not have wise ones and you do not want a curse.
[OUT]It is the strongest who will survive in this world. Attack!
[/NODE]

[/EVENT]

-- [EVENT] --Old humans(4)
+[NODE]2
[STORY]
A group of elderly humans approaches you.
[/STORY]
[OUT]Invite them to join you.
[OUT]Kill them.
[OUT]Kill them.
[OUT]Tell them to move along.
[/NODE]

+[NODE]3
[STORY]
'Ah, thank you, it is good to see some common courtesy in this troubled world. 

I am Professor Wilkoglov, and these are my colleagues. We are scholars, you see. If you let us stay with you awhile and feed us, we will teach you a thing or two.'
[/STORY]
[OUT]Ask more about scholars.
[OUT][Zerca]Say that you are honoured to welcome them. 
[OUT][Elf]Say that you respect their endeavours, yet you remember a time when it was scholars who doomed the world to darkness.
[OUT]Agree. [Lose 30% food]
[OUT]Agree. [Lose 30% food]
[OUT]Decline.
[OUT]Decline.
[/NODE]

+[NODE]4
[STORY]
You kill the elderly folk with no trouble. 

You find books written in a dialect unknown to you and some basic supplies. 

[/STORY]
[OUT]Leave.
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]7
[STORY]
You kill the elderly folk without trouble, but their violent deaths twist their souls into wraiths!

[/STORY]
[OUT]Fight!
[OUT]Face them in the spirit world.
[/NODE]

+[NODE]13
[STORY]
On the path of harmony, such senseless violence taints your spirit. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
You defeat the wraiths and their souls are destroyed.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]19
[STORY]
You defeat the wraiths.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]22
[STORY]
On the path of intellect and magic, knowledge is gold. 

You feel depressed for losing your chance to gain wisdom.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]25
[STORY]
The scholars stay awhile and pass on what knowledge they can before they move on. 

[/STORY]
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]27
[STORY]
The scholars stay awhile and pass on what knowledge they can before they move on. 

They also leave a gift.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]32
[STORY]
After the welcome you gave, one of the wise ones decides to stay with you.
[/STORY]
[OUT]Great, welcome them.
[/NODE]

+[NODE]35
[STORY]
The scholars stay awhile and pass on what knowledge they can before they move on.

[/STORY]
[OUT]Leave.
[OUT]Leave.
[/NODE]

+[NODE]36
[STORY]
The wraiths' primal anger proves overwhelming. Fortunately, in their fresh confusion, they wander off seeking new victims, leaving you behind.
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]39
[STORY]
'Our world was not always in such turmoil. Centuries ago, before darkness came, we had cities, universities!

Now, since creatures of the night hunt our dreams and the light of day seeks to burn our souls while the earth continues to shatter -- well, there's little time for knowledge. Yet we endure. As learned men and women, we gather knowledge and write it down.'
[/STORY]
[OUT]Ask what for.
[/NODE]

+[NODE]40
[STORY]
'For the future, of course. Once the world returns to a semblance of balance, we will once more require knowledge! It is our best weapon, our only saviour, our grace...'

He trails off.
[/STORY]
[OUT]Nod. 
[/NODE]

+[NODE]42
[STORY]
'Ah, a zerca, priest of the great gods of the pantheon. You, my friend, could likely answer many of our quarrels. 

Let us exchange knowledge, then, shall we? And we will require only very basic food and board.'
[/STORY]
[OUT]Agree. [Lose 20% food]
[OUT]Say you cannot afford to lose the food. 
[/NODE]

+[NODE]43
[STORY]
The professor frowns, and the others shake their heads vigorously.

'Propaganda, that is all. It was never quite proven. At least, there are no physical sources to suggest such a direct cause and effect -- perhaps a dash of meddling, some misguided experiments...'

[/STORY]
[OUT]Insist that you know your history. 
[OUT]Let them off. 
[/NODE]

+[NODE]44
[STORY]
'Well, I say, your elven brains are bound to suffer from all that light and stagnation, you know. You have very little turmoil in you. This does not bode well for your progress, thus your minds can muddle the truth, yes...'
[/STORY]
[OUT]Tell them politely that it is their minds that are fogged by age and decay.
[OUT]Tell them you will not tolerate such stupidity and ask them to leave.
[/NODE]

+[NODE]45
[STORY]
'In any case, would you like our schooling or not?'
[/STORY]
[OUT]Agree. [Lose 30% food]
[OUT]Say you cannot afford to lose the food. 
[OUT]Agree. [Lose 30% food]
[/NODE]

+[NODE]47
[STORY]
'If you let us stay with you awhile and feed us, we will teach you a thing or two.'
[/STORY]
[OUT]Ask more about scholars.
[OUT][Zerca]Say that you are honoured to welcome them. 
[OUT][Elf]Say that you respect their endeavours, yet you remember a time when it was scholars who doomed the world to darkness.
[OUT]Agree. [Lose 30% food]
[OUT]Agree. [Lose 30% food]
[OUT]Decline.
[OUT]Decline.
[/NODE]

+[NODE]48
[STORY]
'Yes, of course, hard times everywhere. Well, then, we will rest and move on. May wisdom guide your path.
[/STORY]
[OUT]Leave.
[/NODE]

[/EVENT]

-- [EVENT] --EncounterRacesDiff5(5)
+[NODE]2
[STORY]
Someone approaches you.
[/STORY]
[OUT]Stay alert.
[OUT]Stay alert.
[OUT]Stay alert.
[/NODE]

[/EVENT]

-- [EVENT] --Goblin travelersDiff2(6)
+[NODE]2
[STORY]
As you're about to set up camp, a group of goblin travellers approaches, asking for shelter. 
[/STORY]
[OUT][Goblin]Greet your kin and welcome them to your camp.
[OUT][Orc]Intimidate them into giving you their stuff or else you'll attack.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT][Harmony]It is the way of harmony to help. Invite them and share food.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Welcome them to your camp. We should be helping one another.
[OUT]Attack!
[OUT]Refuse. You will not share bread with strangers. 
[OUT]Refuse. You will not share bread with strangers. 
[OUT]Refuse. You will not share bread with strangers. 
[/NODE]

+[NODE]3
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]6
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

There is a chance the spirits imparted wisdom to you as well. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]8
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

And then you realise you drifted off while they robbed you and ran off!
[/STORY]
[OUT]Catch them and kill them!
[OUT]Damned thieves. Leave.
[/NODE]

+[NODE]10
[STORY]
You kill the goblins and loot their bodies.

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]11
[STORY]
The goblins beat you and take some of your things, but they leave you alive, at least. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]18
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

There is a chance the spirits imparted wisdom to you as well. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]24
[STORY]
It is wise to stay cautious. You feel your domain strengthen. 
[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]26
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

They like your company so much, they give you a stash of the weed and one of their puppies before they leave. 

[/STORY]
[OUT]Thank them.
[/NODE]

+[NODE]28
[STORY]
'Oi, oi, easy there, fellas, easy. Here, have some of our stuff. And we, just, we go over there, yeah?'

As the goblins walk off, you hear a lot of swearing.
[/STORY]
[OUT]Take their stuff and continue setting up camp.
[/NODE]

+[NODE]31
[STORY]
'Oh, yes, here we go. You big, you strong, you beat up the gobbos, make you feel better, eh? Stupid oafs. 

We leave you be!'

They walk off. 
[/STORY]
[OUT]Let them go.
[OUT]Attack!
[/NODE]

+[NODE]34
[STORY]
The goblins sit by the fire and share stories with you. They pass around a strange weed, and soon you see visions of the spirits they speak of.

You realise, however, that the stuff they shared was pretty strong, and you all feel very ill. 

[/STORY]
[OUT]Leave.
[/NODE]

+[NODE]35
[STORY]
You refuse and they walk off, but in the morning,  you realise they must have sneaked back and stolen some of your stuff!
[/STORY]
[OUT]Catch them and kill them!
[OUT]Damned thieves. Leave.
[/NODE]

[/EVENT]

