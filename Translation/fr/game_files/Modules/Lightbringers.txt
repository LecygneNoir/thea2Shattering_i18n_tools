-- [EVENT] --VillageSmall(0)
+[NODE]2
[STORY]
Vous arrivez à proximité d'un campement qui a été corrompu par la lumière. Vous apercevez des individus déambuler de part et d'autre, travaillant, fabriquant toute sorte d'objets, labourant les champs, et ce en demeurant silencieux.
Soudain, ils vous aperçoivent et décident de vous interpeller :
« Êtes-vous venus afin de vous joindre à nous ? »
[/STORY]
[OUT]Répondre par la négative, puis leur déclarer que vous êtes là afin de discuter, et éventuellement sympathiser.
[/NODE]

+[NODE]3
[STORY]
« Soit vous faites parties des nôtres, soit vous ne le faites pas. Approchez… »
Des feux follets vous encerclent petit à petit. Vous sentez leur chaleur glaciale entrer en contact avec votre peau, ainsi que d'ardentes stalactites qui effleurent vos tripes alors qu'ils tentent de s'unir à vous.
[/STORY]
[OUT]Décliner et vous en aller en vitesse.
[OUT][Tourmente ou Lumière] Vous vous tenez en face d'une abomination qui défie la Tourmente (en cherchant à la dompter) aussi bien que la Lumière (en abusant de son pouvoir) : attaquer en fonçant tête baissée !
[OUT][Magie ou Nature] L'entité qui se tient devant vous n'a rien de matériel : faire appel à votre domaine afin d'affronter spirituellement ces monstrueuses créatures.
[OUT][Harmonie ou Intellect] Leur donner un aperçu de l'étendue de vos facultés mentales et de l'inflexibilité de votre volonté.
[/NODE]

+[NODE]6
[STORY]
Vous triomphez des porte-lumière, et le village prend soudain feu — la lumière refusant de vous concéder une victoire totale.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]8
[STORY]
Vous êtes battus à plate couture et devez fuir. Certains des vôtres n'ont su résister à l'appel de la lumière…
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]13
[STORY]
La résistance dont vous faites preuve affaiblit la force qui contrôle les porte-lumière, lesquels parviennent à bannir la lumière dont sont empreintes leurs âmes ! Hélas, leurs corps sont trop faibles et trop exténués pour survivre sans l'esprit-lumière. Bien qu'ils aient été libérés, vous les voyez s'effondrer au sol.
Le village prend soudain feu, comme si la lumière refusait de vous accorder votre victoire.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]18
[STORY]
« Porteur de la lumière ! Approchez, et restez un instant. Bien que je ne doute pas que vous fassiez un jour partie des nôtres, il se peut que certains d'entre nous souhaitent se joindre à vous dans l'immédiat. »
[/STORY]
[OUT]Faire un tour au village lumineux.
[/NODE]

+[NODE]19
[STORY]
La bourgade semble figée dans le temps : vous voyez les gens se déplacer avec un telle lenteur qu'ils semblent presque immobiles. Et pourtant, ils affichent tous un air heureux.
[/STORY]
[OUT]Faire soigner votre esprit.
[OUT]Faire restaurer votre santé mentale.
[OUT]Lever des malédictions.
[OUT]Recruter des porte-lumière.
[OUT]Il n'y a rien à faire pour le moment : partir.
[/NODE]

+[NODE]32
[STORY]
The lightbringers turn to you in anger, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --ArmyLight(1)
+[NODE]2
[STORY]
Un groupe d'individus envoûtés par la lumière — des porte-lumière, pour les appeler par leur nom — s'approche de vous.
Ils ne vous décrochent aucun mot, et veulent seulement vous inviter au sein de la communauté sous laquelle l'esprit-lumière les réunit.
[/STORY]
[OUT]Leur adresser la parole.
[OUT]Combattre !
[OUT]Les affronter à l'aide de votre esprit !
[OUT]S'éloigner.
[/NODE]

+[NODE]17
[STORY]
Vous triomphez des porte-lumières, et leurs corps s'effritent pour ne former plus qu'un tas de cendres.
[/STORY]
[OUT]Récupérer leurs affaires et partir.
[/NODE]

+[NODE]19
[STORY]
Votre esprit s'avère trop faible : les porte-lumière vous terrassent, et c'est tout juste si vous parvenez à vous échapper — vous n'avez d'ailleurs peut-être pas tous réussi à vous enfuir…
[/STORY]
[OUT]Courir.
[/NODE]

+[NODE]27
[STORY]
« Vous… vous avez été généreux envers la lumière. Vous nous rejoindrez lorsque vous serez prêts… »
Ils s'en vont.
[/STORY]
[OUT]Attaquer !
[OUT]Partir.
[/NODE]

+[NODE]30
[STORY]
Un groupe de porte-lumière s'approche de vous, mais ils s'effondrent brusquement au sol. L'anéantissement de la bête-lumière est en train de faire effet.
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --VillageLarge(2)
+[NODE]2
[STORY]
Vous arrivez à proximité d'un campement qui a été corrompu par la lumière. Vous apercevez des individus déambuler de part et d'autre, travaillant, fabriquant toute sorte d'objets, labourant les champs, et ce en demeurant silencieux.
Soudain, ils vous aperçoivent et décident de vous interpeller :
« Êtes-vous venus afin de vous joindre à nous ? »
[/STORY]
[OUT]Répondre par la négative, puis leur déclarer que vous êtes là afin de discuter, et éventuellement sympathiser.
[/NODE]

+[NODE]3
[STORY]
« Soit vous faites parties des nôtres, soit vous ne le faites pas. Approchez… »
Des feux follets vous encerclent petit à petit. Vous sentez leur chaleur glaciale entrer en contact avec votre peau, ainsi que d'ardentes stalactites qui effleurent vos tripes alors qu'ils tentent de s'unir à vous.
[/STORY]
[OUT]Décliner et vous en aller en vitesse.
[OUT][Tourmente ou Lumière] Vous vous tenez en face d'une abomination qui défie la Tourmente (en cherchant à la dompter) aussi bien que la Lumière (en abusant de son pouvoir) : attaquer en fonçant tête baissée !
[OUT][Magie ou Nature] L'entité qui se tient devant vous n'a rien de matériel : faire appel à votre domaine afin d'affronter spirituellement ces monstrueuses créatures.
[OUT][Harmonie ou Intellect] Leur donner un aperçu de l'étendue de vos facultés mentales et de l'inflexibilité de votre volonté.
[/NODE]

+[NODE]6
[STORY]
Vous triomphez des porte-lumière, et le village prend soudain feu — la lumière refusant de vous concéder une victoire totale.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]8
[STORY]
Vous êtes battus à plate couture et devez fuir. Certains des vôtres n'ont su résister à l'appel de la lumière…
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]13
[STORY]
La résistance dont vous faites preuve affaiblit la force qui contrôle les porte-lumière, lesquels parviennent à bannir la lumière dont sont empreintes leurs âmes ! Hélas, leurs corps sont trop faibles et trop exténués pour survivre sans l'esprit-lumière. Bien qu'ils aient été libérés, vous les voyez s'effondrer au sol.
Le village prend soudain feu, comme si la lumière refusait de vous accorder votre victoire.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]18
[STORY]
« Porteur de la lumière ! Approchez, et restez un instant. Bien que je ne doute pas que vous fassiez un jour partie des nôtres, il se peut que certains d'entre nous souhaitent se joindre à vous dans l'immédiat. »
[/STORY]
[OUT]Faire un tour au village lumineux.
[/NODE]

+[NODE]19
[STORY]
La bourgade semble figée dans le temps : vous voyez les gens se déplacer avec un telle lenteur qu'ils semblent presque immobiles. Et pourtant, ils affichent tous un air heureux.
[/STORY]
[OUT]Faire soigner votre esprit.
[OUT]Faire restaurer votre santé mentale.
[OUT]Lever des malédictions.
[OUT]Recruter des porte-lumière.
[OUT]Il n'y a rien à faire pour le moment : partir.
[/NODE]

[/EVENT]

-- [EVENT] --Village Very Small(3)
+[NODE]2
[STORY]
Vous arrivez à proximité d'un campement qui a été corrompu par la lumière. Vous apercevez des individus déambuler de part et d'autre, travaillant, fabriquant toute sorte d'objets, labourant les champs, et ce en demeurant silencieux.
Soudain, ils vous aperçoivent et décident de vous interpeller :
« Êtes-vous venus afin de vous joindre à nous ? »
[/STORY]
[OUT]Répondre par la négative, puis leur déclarer que vous êtes là afin de discuter, et éventuellement sympathiser.
[/NODE]

+[NODE]3
[STORY]
« Soit vous faites parties des nôtres, soit vous ne le faites pas. Approchez… »
Des feux follets vous encerclent petit à petit. Vous sentez leur chaleur glaciale entrer en contact avec votre peau, ainsi que d'ardentes stalactites qui effleurent vos tripes alors qu'ils tentent de s'unir à vous.
[/STORY]
[OUT]Décliner et vous en aller en vitesse.
[OUT][Tourmente ou Lumière] Vous vous tenez en face d'une abomination qui défie la Tourmente (en cherchant à la dompter) aussi bien que la Lumière (en abusant de son pouvoir) : attaquer en fonçant tête baissée !
[OUT][Magie ou Nature] L'entité qui se tient devant vous n'a rien de matériel : faire appel à votre domaine afin d'affronter spirituellement ces monstrueuses créatures.
[OUT][Harmonie ou Intellect] Leur donner un aperçu de l'étendue de vos facultés mentales et de l'inflexibilité de votre volonté.
[/NODE]

+[NODE]6
[STORY]
Vous triomphez des porte-lumière, et le village prend soudain feu — la lumière refusant de vous concéder une victoire totale.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]8
[STORY]
Vous êtes battus à plate couture et devez fuir. Certains des vôtres n'ont su résister à l'appel de la lumière…
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]13
[STORY]
La résistance dont vous faites preuve affaiblit la force qui contrôle les porte-lumière, lesquels parviennent à bannir la lumière dont sont empreintes leurs âmes ! Hélas, leurs corps sont trop faibles et trop exténués pour survivre sans l'esprit-lumière. Bien qu'ils aient été libérés, vous les voyez s'effondrer au sol.
Le village prend soudain feu, comme si la lumière refusait de vous accorder votre victoire.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]18
[STORY]
« Porteur de la lumière ! Approchez, et restez un instant. Bien que je ne doute pas que vous fassiez un jour partie des nôtres, il se peut que certains d'entre nous souhaitent se joindre à vous dans l'immédiat. »
[/STORY]
[OUT]Faire un tour au village lumineux.
[/NODE]

+[NODE]19
[STORY]
La bourgade semble figée dans le temps : vous voyez les gens se déplacer avec un telle lenteur qu'ils semblent presque immobiles. Et pourtant, ils affichent tous un air heureux.
[/STORY]
[OUT]Faire soigner votre esprit.
[OUT]Faire restaurer votre santé mentale.
[OUT]Lever des malédictions.
[OUT]Recruter des porte-lumière.
[OUT]Il n'y a rien à faire pour le moment : partir.
[/NODE]

+[NODE]32
[STORY]
The lightbringers turn to you in anger, but they suddenly simply collapse. The destruction of the light-beast is working.
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --Nest(4)
+[NODE]2
[STORY]
You come to a place claimed by the light. There are creatures and people here, all standing together, staring into space.
When they spot you, they approach:
'You have come to join us?'
[/STORY]
[OUT]Répondre par la négative, puis leur déclarer que vous êtes là afin de discuter, et éventuellement sympathiser.
[/NODE]

+[NODE]3
[STORY]
« Soit vous faites parties des nôtres, soit vous ne le faites pas. Approchez… »
Des feux follets vous encerclent petit à petit. Vous sentez leur chaleur glaciale entrer en contact avec votre peau, ainsi que d'ardentes stalactites qui effleurent vos tripes alors qu'ils tentent de s'unir à vous.
[/STORY]
[OUT]Décliner et vous en aller en vitesse.
[OUT][Tourmente ou Lumière] Vous vous tenez en face d'une abomination qui défie la Tourmente (en cherchant à la dompter) aussi bien que la Lumière (en abusant de son pouvoir) : attaquer en fonçant tête baissée !
[OUT][Magie ou Nature] L'entité qui se tient devant vous n'a rien de matériel : faire appel à votre domaine afin d'affronter spirituellement ces monstrueuses créatures.
[OUT][Harmonie ou Intellect] Leur donner un aperçu de l'étendue de vos facultés mentales et de l'inflexibilité de votre volonté.
[/NODE]

+[NODE]6
[STORY]
You destroy the lightbringers and the nest burns, as if the light will not allow you a full victory.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]8
[STORY]
Vous êtes battus à plate couture et devez fuir. Certains des vôtres n'ont su résister à l'appel de la lumière…
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]13
[STORY]
Your resistance wakens something within the taken folk and they banish the light from their souls! Alas, their bodies are too weak and burnt out to survive without the hive mind and they fall dead, but free.
The nest itself burns as if the light would not abide your victory.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]18
[STORY]
'Bringer of light, come, see us. One day you will join. For now we thank you for your aid.'
[/STORY]
[OUT]Les laisser tranquilles.
[OUT]Attaquer !
[/NODE]

+[NODE]32
[STORY]
Un groupe de porte-lumière s'approche de vous, mais ils s'effondrent brusquement au sol. L'anéantissement de la bête-lumière est en train de faire effet.
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

