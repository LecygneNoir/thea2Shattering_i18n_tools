-- [EVENT] --NightdemonsTradeadv(0)
+[NODE]2
[STORY]
	Vous vous trouvez devant les étals du marchand.
[/STORY]
[OUT]Échanger.
[OUT]Échanger.
[OUT]Échanger.
[OUT]Revenir à un autre moment.
[/NODE]

+[NODE]7
[STORY]
« Vous ne faites pas partie de nos amis, il y aura une petite taxe supplémentaire. »
[/STORY]
[OUT]Échanger.
[OUT]Revenir à un autre moment.
[/NODE]

+[NODE]11
[STORY]
« Vous nous faites perdre notre temps. Allez-vous en ! »
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --NightdemonsVillage(1)
+[NODE]2
[STORY]
Vous entrez dans le territoire revendiqué par les démons de la nuit. Vous voyez des loups-garous et des strigas rôder dans les ombres, vous regardant avec des yeux gourmands : 
« Êtes-vous venus pour offrir votre délicieux sang, ou votre chair peut-être ? »
[/STORY]
[OUT][Horz ou Zorya] En tant que maîtres de la lune et des étoiles , vous revendiquez votre place auprès des enfants de la nuit.
[OUT]Demandez si vous pouvez entrer et explorer ?
[OUT]Partir.
[OUT][Ami ou démon de la nuit] Dire que vous acceptez tous les dangers qui pourraient vous frapper ici et que vous souhaitez entrer quand même.
[/NODE]

+[NODE]3
[STORY]
Vous entrez dans le territoire revendiqué par les démons de la nuit. Vous sentez que de nombreux yeux sont posés sur vous et un grognement sourd vous accueille :
 « Êtes-vous venus étancher notre soif de sang, ennemis de la nuit ? »
[/STORY]
[OUT]Attaquer !
[OUT]Attaquer !
[OUT]Partir.
[/NODE]

+[NODE]4
[STORY]
Les habitants de l'endroit commencent à vous encercler et vous vous sentez vulnérables à leur appétit.
« Vous portez de l'argent, du mithril ou d'autres objets nés de métaux honnis et vous osez venir en ce lieu. Partez ! » 
[/STORY]
[OUT]Mieux vaut partir.
[OUT]Attaquer !
[OUT]Attaquer !
[/NODE]

+[NODE]7
[STORY]
« Vous êtes des amis, pour le moment. Vous pouvez donc entrer, mais à vos risques et périls. »
[/STORY]
[OUT]Demander s’il y a quelque chose d’intéressant dans le coin. 
[OUT]Demander s’ils veulent commercer avec vous.
[OUT]Il y a quelques loups-garous dans un cercle de combat. Aller les voir.
[OUT]Visiter la maison des plaisirs.
[/NODE]

+[NODE]11
[STORY]
« J'accepte votre offre, je vais invoquer des morts-vivants à votre service. »
[/STORY]
[OUT]Partir.
[OUT]Partir.
[/NODE]

+[NODE]12
[STORY]
« Vous nous avez prouvé votre amitié jusqu’ici, donc oui, nous commercerons avec vous. »
[/STORY]
[OUT]Échanger.
[/NODE]

+[NODE]15
[STORY]
Les bêtes de la nuit se combattent pour prouver leur valeur. Vous êtes invités à vous joindre à elles, si vous l'osez. 
Non loin, un maître striga exhibe ses pouvoirs mystiques.
[/STORY]
[OUT][Seulement une fois] Tester votre valeur au combat contre un loup-garou. Deux des plus forts d'entre vous vont se battre.
[OUT][Seulement une fois] Tester votre valeur au combat contre un loup-garou. L'un des plus forts d'entre vous va se battre.
[OUT]Vous avez épuisé vos chances de lancer un défi, ou ce n'est simplement pas votre jour. Repartir.
[OUT][Seulement une fois] Défier le maître striga, en un contre un.
[/NODE]

+[NODE]16
[STORY]
« C'est votre jour de chance : vous pouvez entrer, à vos risques et périls. »
[/STORY]
[OUT]Demander s’ils veulent commercer avec vous.
[OUT]Visiter la maison des plaisirs.
[OUT]La maison des plaisirs est occupée, revenir plus tard.
[/NODE]

+[NODE]18
[STORY]
Vous entrez dans les sombres pièces de la maison des plaisirs et un danseur exotique, moitié de chair et moitié d'esprit, se charge de vous distraire.
Votre esprit est restauré, mais vous sentez qu'une partie de votre sang a été drainé, ce qui vous laisse souffrant.
[/STORY]
[OUT]Les remercier, se reposer et partir.
[/NODE]

+[NODE]19
[STORY]
Vous vous êtes bourré la gueule dans la maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures, fruit d'une soirée d'« amour bestial »... mais vous vous sentez ragaillardi. 
[/STORY]
[OUT]Les remercier, se reposer et partir.
[/NODE]

+[NODE]20
[STORY]
Vous vous êtes bourré la gueule dans la maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures, fruit d'une soirée d'« amour bestial »... Vous ne vous sentez pas très bien, mais c'était une nuit mémorable. Enfin, elle l'était... n'est-ce pas ?
[/STORY]
[OUT]Partir...
[/NODE]

+[NODE]21
[STORY]
Vous vous êtes bourré la gueule dans la maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures, fruit d'une soirée d'« amour bestial »... Et maintenant l'un des loups pense que vous êtes son papa...
[/STORY]
[OUT]Partir...
[/NODE]

+[NODE]22
[STORY]
Vous vous êtes bourré la gueule dans la maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures, fruit d'une soirée d'« amour bestial »... et quand vous vous regardez, vous êtes transformé en striga !
[/STORY]
[OUT]Partir...
[/NODE]

+[NODE]28
[STORY]
Vous avez massacré les démons de la nuit et vous entendez un hurlement glaçant dans le lointain...
Mais après tout,  n'êtes-vous pas êtes le champion de Horz et un Alpha de la lune ? Vous êtes en droit de revendiquer la vie de ces bêtes si c'est votre bon vouloir. Ceux qui ont été témoins de la scène vous respecteront.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]31
[STORY]
Les démons de la nuit vous ont vaincus et réclament le prix du sang pour votre attaque.
[/STORY]
[OUT]Partir.
[OUT]Partir.
[/NODE]

+[NODE]36
[STORY]
« Non, partez. Revenez avec des gens qui sont des amis de la nuit, peut-être qu'alors nous écouterons. »
[/STORY]
[OUT]Mieux vaut partir.
[OUT]Attaquer !
[OUT]Attaquer !
[/NODE]

+[NODE]37
[STORY]
« C'est votre jour de chance, vous dites que la nature est votre maîtresse et nous respectons ça. Vous pouvez entrer, à vos risques et périls. »
[/STORY]
[OUT]Demander s’ils veulent commercer avec vous.
[OUT]Visiter la maison des plaisirs.
[/NODE]

+[NODE]38
[STORY]
Depuis les ombres, elles vous parlent : 
« Vous êtes un Élu de la nuit. Nous voulons rejoindre votre meute. »
[/STORY]
[OUT]Accepter quiconque veut se joindre à vous.
[OUT]Refuser poliment.
[/NODE]

+[NODE]44
[STORY]
Vous avez massacré les démons de la nuit  et vous entendez un hurlement glaçant dans le lointain...
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]48
[STORY]
Le loup-garou acclame votre force et vous donne des trophées en récompense de votre victoire.
Sa meute hurle pour vous honorer.
[/STORY]
[OUT]Proposer à votre opposant de boire pour célébrer un bien beau combat.
[OUT]Demander que l'un des vôtres soit changé en enfant de la lune, comme lui !
[OUT]Demander que l'un des vôtres soit changé en enfant de la lune, comme lui !
[/NODE]

+[NODE]49
[STORY]
Les démons de la nuit se regroupent et s'inclinent avant  le défi. Un loup-garou entre dans le cercle et montre ses dents coupantes comme des rasoirs en guise d'invitation.
[/STORY]
[OUT]Commencer.
[/NODE]

+[NODE]50
[STORY]
Vous êtes défaits et devez concéder la victoire en courbant l'échine devant le gagnant.
« Vous vous êtes bien battus, mais vous n'êtes pas encore un véritable enfant de la nuit. Maintenant, si vous tenez encore debout, allons boire ensemble ! »
[/STORY]
[OUT]Boire, si vous le pouvez encore...
[/NODE]

+[NODE]52
[STORY]
L'un des combattants se réveillera à la prochaine pleine lune changé en enfant de la nuit !
[/STORY]
[OUT]Louer Horz.
[/NODE]

+[NODE]56
[STORY]
« Je suis d’accord, vous méritez de rejoindre nos rangs, pour Horz ! » 
L’un des combattants se réveillera à la prochaine pleine lune changé en enfant de la nuit !
[/STORY]
[OUT]Louer Horz.
[/NODE]

+[NODE]57
[STORY]
« Vous abandonnez ? Trouillard, hors de ma vue ! »
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]58
[STORY]
Les démons de la nuit se regroupent et s'inclinent avant  le défi. Un loup-garou entre dans le cercle et montre ses dents coupantes comme des rasoirs en guise d'invitation.
[/STORY]
[OUT]Commencer.
[/NODE]

+[NODE]60
[STORY]
Vous êtes défaits et devez concéder la victoire en courbant l'échine devant le gagnant.
« Vous vous êtes bien battus, mais vous n'êtes pas encore un véritable enfant de la nuit. Maintenant, si vous tenez encore debout, allons boire ensemble ! »
[/STORY]
[OUT]Boire, si vous le pouvez encore...
[/NODE]

+[NODE]62
[STORY]
L'un des combattants se réveillera à la prochaine pleine lune changé en enfant de la nuit !
[/STORY]
[OUT]Louer Horz.
[/NODE]

+[NODE]63
[STORY]
Le loup-garou acclame votre force et vous donne des trophées en récompense de votre victoire.
Sa meute hurle pour vous honorer.
[/STORY]
[OUT]Proposer à votre opposant de boire pour célébrer un bien beau combat.
[OUT]Demander à être changé en enfant de la lune, comme lui !
[/NODE]

+[NODE]64
[STORY]
« Je suis d’accord, vous méritez de rejoindre nos rangs, pour Horz ! » 
L’un des combattants se réveillera à la prochaine pleine lune changé en enfant de la nuit !
[/STORY]
[OUT]Louer Horz.
[/NODE]

+[NODE]65
[STORY]
Il y a un nécromant qui réside dans le village. Il peut vous offrir votre propre compagnon mort-vivant, pour un certain prix.
[/STORY]
[OUT]Offrir des familiers en payement (vous devez en avoir 15).
[OUT]Offrir un enfant.
[OUT]Offrir un membre de la compagnie, adulte ou enfant.
[/NODE]

+[NODE]68
[STORY]
« Hmmm, ce n'est pas mon tarif habituel, mais cela fera l'affaire, pour cette fois. »
[/STORY]
[OUT]Payer.
[/NODE]

+[NODE]70
[STORY]
« Vous avez enchanté mon cœur avec vos talents digne d'un maître. Je m'incline devant votre habileté exceptionnelle et votre beauté à couper le souffle.
Voulez-vous devenir ma maîtresse de la nuit ? Une douce morsure et votre monde sera changé pour toujours ! »
[/STORY]
[OUT][Elfe] Hélas, les elfes ne peuvent accepter un si sombre présent. Le remercier néanmoins.
[OUT]Accepter l'offre.
[OUT]Décliner poliment et partir.
[OUT]Accepter l'offre.
[/NODE]

+[NODE]71
[STORY]
« Me défier ?  Que c'est plaisant, dansons ma chère ! »
[/STORY]
[OUT]Commencer.
[/NODE]

+[NODE]72
[STORY]
'Ah, yes, children of light. Such a shame, still. I am enamoured and so shall gift you a night-kiss so that it may remind you of me. From now on, you shall be part of our night pack, even if only a little. Now fare thee well, my love!'
[/STORY]
[OUT]Le remercier et partir, cela devient gênant.
[/NODE]

+[NODE]74
[STORY]
Vous devenez une maîtresse de la nuit. Le maître striga est épuisé par le rituel, il est temps maintenant de s'éclipser.
[/STORY]
[OUT]Partir, mais promettre de revenir le voir...
[/NODE]

+[NODE]75
[STORY]
« Merveilleux, oui, votre talent est grand, je suis impressionné. Je m'incline devant votre supériorité, mon amie. Voilà une récompense pour ce défi. »
[/STORY]
[OUT]Le remercier et partir.
[OUT]Demander à être transformer en maître striga.
[OUT]Demander à être changer en striga.
[/NODE]

+[NODE]76
[STORY]
« Oui, vous deviendrez mon égal. Accordé ! »
[/STORY]
[OUT]Le remercier et partir.
[/NODE]

+[NODE]77
[STORY]
« Oui, vous deviendrez mon enfant. Accordé ! »
[/STORY]
[OUT]Le remercier et partir.
[/NODE]

+[NODE]78
[STORY]
« Et ainsi vous vous soumettez face à ma supériorité, bien... »
[/STORY]
[OUT]Accepter votre défaite.
[OUT]Accepter votre défaite.
[OUT]Accepter votre défaite.
[/NODE]

+[NODE]79
[STORY]
« Malgré votre échec, vous avez enchanté mon cœur par votre beauté à couper le souffle.  Vous deviendrez ma maîtresse de la nuit. Une douce morsure et votre monde sera changé pour toujours ! »
Vous devenez une maîtresse de la nuit. Le maître striga est épuisé par le rituel, vous en profitez pour vous éclipser.
[/STORY]
[OUT]Vous ne semblez pas vraiment  avoir le choix, acquiescer. 
[/NODE]

+[NODE]81
[STORY]
« Ainsi, vous deviendrez un de mes enfants, et vous servirez la nuit. »
[/STORY]
[OUT]Vous ne semblez pas vraiment  avoir le choix, acquiescer. 
[/NODE]

+[NODE]82
[STORY]
Vous vous êtes bourré la gueule dans la maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures, fruits d'une soirée d'« amour bestial »... et quand vous vous regardez, vous êtes transformé en loup-garou !
[/STORY]
[OUT]Partir...
[/NODE]

+[NODE]83
[STORY]
Vous vous êtes bourré la gueule  dans une maison des plaisirs des démons de la nuit. Vous vous réveillez couvert de fourrures et de morsures,  fruits d'une soirée d'« amour bestial »...
[/STORY]
[OUT]Partir...
[/NODE]

+[NODE]84
[STORY]
« Vous nous avez prouvé votre amitié jusqu'ici, donc oui, nous commercerons avec vous. »
[/STORY]
[OUT]Échanger.
[/NODE]

+[NODE]85
[STORY]
« Oui, des compagnons de la nuit liés pour toujours, et l'un d'entre vous est un alpha, cool. Entrez, profitez de la nuit ! » 
[/STORY]
[OUT]Demander s’il y a quelque chose d’intéressant dans le coin. 
[OUT]Demander s’ils veulent commercer avec vous.
[OUT]Il y a quelques loups-garous dans un cercle de combat. Aller les voir.
[OUT]Visiter la maison des plaisirs.
[OUT]Un petit groupe d'enfants de la nuit vous regarde intensément. Les approcher.
[/NODE]

+[NODE]87
[STORY]
« Non, pas de transaction, c'est le moment de partir ? »
[/STORY]
[OUT]Mieux vaut partir.
[OUT]Visiter la maison des plaisirs.
[OUT]Il y a quelques loups-garous dans un cercle de combat. Aller les voir.
[OUT]Demander s’ils veulent commercer avec vous.
[/NODE]

+[NODE]90
[STORY]
Quelqu'un que vous aviez perdu au profit des démons de la nuit vous revient, quelque peu changé toutefois.
[/STORY]
[OUT]Le prendre avec vous et partir.
[/NODE]

+[NODE]92
[STORY]
« Ah, il semble que vous ne soyez devenu qu’un enfant de la nuit mineur ; toutefois, nous sommes liés maintenant ! »
[/STORY]
[OUT]Partir, mais promettre de revenir le voir...
[/NODE]

+[NODE]93
[STORY]
« Je suis d’accord, vous méritez de rejoindre nos rangs, pour Horz ! » 
L’un des combattants se réveillera à la prochaine pleine lune changé en enfant de la nuit !
[/STORY]
[OUT]Louer Horz.
[/NODE]

+[NODE]95
[STORY]
« D'accord. »
[/STORY]
[OUT]Échanger.
[/NODE]

[/EVENT]

-- [EVENT] --ArmyNightDemon_(2)
+[NODE]2
[STORY]
Les bêtes de la nuit vous traquent !
[/STORY]
[OUT]Saluer vos alliés.
[OUT]Rester prudents, mais leur parler.
[OUT]Utiliser de l’argent, ou un meilleur métal, pour les intimider et les forcer à battre en retraite !
[OUT]Combattre !
[OUT]S'éloigner.
[/NODE]

+[NODE]3
[STORY]
« Toutes mes excuses, chers amis. Nous ne vous avions pas reconnus. Bienvenue et bonne chance. »
[/STORY]
[OUT]Demander si ils veulent marchander.
[OUT]Partir.
[/NODE]

+[NODE]11
[STORY]
Ils ne répondent pas et continuent de s'approcher.
[/STORY]
[OUT]Partir.
[OUT]Utiliser de l’argent, ou un meilleur métal, pour les intimider et les forcer à battre en retraite !
[OUT]Combattre !
[OUT]Faire face aux bêtes de la nuit avec votre force spirituelle pour gagner leur respect.
[/NODE]

+[NODE]17
[STORY]
Vous battez les bêtes de la nuit.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]19
[STORY]
Les bêtes de la nuit vous battent et se repaissent de votre sang et de votre essence vitale avant que vous n'arriviez à vous échapper.
[/STORY]
[OUT]Courir.
[/NODE]

+[NODE]25
[STORY]
Les démons de la nuit  battent en retraite et regagnent les ombres.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]28
[STORY]
Vous  vainquez les bêtes de la nuit et elles sont impressionnées par votre compréhension des voies mystiques.
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

