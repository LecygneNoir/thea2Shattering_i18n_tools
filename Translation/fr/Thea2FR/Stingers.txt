-- [EVENT] --StingersTradesmall(0)
+[NODE]2
[STORY]
Les Insectes vous montrent ce qu'ils ont à échanger.
[/STORY]
[OUT]Échanger.
[OUT]Échanger.
[OUT]Échanger.
[OUT]Revenir une autre fois.
[/NODE]

+[NODE]7
[STORY]
« Nous pas connaître vous, alors vous payer d'abord. »
[/STORY]
[OUT]Échanger.
[OUT]Revenir une autre fois.
[/NODE]

+[NODE]11
[STORY]
« Une bonne affaire. Vous bourdonner ailleurs maintenant. »
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --StingersTradeadv(1)
+[NODE]2
[STORY]
Les Insectes vous montrent ce qu'ils ont à échanger.
[/STORY]
[OUT]Échanger.
[OUT]Échanger.
[OUT]Échanger.
[OUT]Revenir une autre fois.
[/NODE]

+[NODE]7
[STORY]
« Nous pas vous connaître. Vous payer une commission d'abord. »
[/STORY]
[OUT]Échanger.
[OUT]Revenir une autre fois.
[/NODE]

+[NODE]11
[STORY]
« Une bonne affaire. Vous bourdonner ailleurs maintenant. »
[/STORY]
[OUT]Partir.
[/NODE]

[/EVENT]

-- [EVENT] --Army Stingers(2)
+[NODE]2
[STORY]
Vous entendez le bourdonnement menaçant d'une patrouille d'Insectes juste derrière vous !
[/STORY]
[OUT]Saluer vos alliés.
[OUT]Les aborder en restant prudents.
[OUT]Essayer de vous échapper.
[OUT]Combattre !
[OUT]S'éloigner.
[OUT]Les soudoyer afin de gagner leurs faveurs.
[/NODE]

+[NODE]3
[STORY]
« Vous gentils. Nous pas taper. »
[/STORY]
[OUT]Leur demander s'ils veulent marchander.
[OUT]Échanger de la nourriture en faisant preuve de respect.
[/NODE]

+[NODE]11
[STORY]
Ils ne répondent pas et continuent de s'approcher.
[/STORY]
[OUT]Partir.
[OUT]Essayer de vous échapper.
[OUT]Combattre !
[/NODE]

+[NODE]17
[STORY]
Vous triomphez des Insectes.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]19
[STORY]
Les Insectes vous vainquent et vous infectent avec une toxine avant que vous ne parveniez à vous échapper.
[/STORY]
[OUT]Courir.
[/NODE]

+[NODE]25
[STORY]
Les Insectes s'en vont après avoir reconnu votre supériorité.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]30
[STORY]
Les Insectes vous ignorent complètement. Pire encore, ils en profitent pour vous mordre alors que vous êtes distraits et vous injectent une toxine avant que vous ne parveniez à vous échapper.
[/STORY]
[OUT]Courir.
[/NODE]

[/EVENT]

-- [EVENT] --Stingers Lair_(3)
+[NODE]3
[STORY]
Vous découvrez un foyer bourdonnant au cœur duquel s'agitent de nombreux insectes. Cela vous fait quelque peu penser à une grosse ruche d'abeilles, jusqu'à ce que vous remarquiez plusieurs canaux souterrains ainsi que d'autres nids qui semblent tous connectés les uns aux autres afin de former une seule grande colonie.
Plusieurs abeilles s'approchent et vous adressent la parole :
« Vous pas animaux. Vous bagarrer ou échanger ? »
[/STORY]
[OUT]Essayer de leur répondre.
[OUT]Attaquer !
[/NODE]

+[NODE]4
[STORY]
Le repaire bourdonne de vie, mais les insectes semblent peu accueillants.
Vous pouvez échanger, soigner vos toxines, et il se peut que quelques araignées acceptent de vous rejoindre — moyennant paiement.
Lorsque vos relations seront amicales, vous pourrez recruter des abeilles, des reines araignées, ou entraîner vos bêtes afin de les rendre plus fortes, plus endurantes, ou plus sages.
[/STORY]
[OUT]Échanger.
[OUT]Partir.
[OUT]Attaquer !
[OUT][Bête] Voir si vous pouvez vous entraîner.
[OUT]Quelque chose est agrippé à votre jambe : baisser les yeux.
[OUT]Offrir de la nourriture afin que les serpents purgent vos toxines.
[OUT]Acheter une araignée rare [Tous les 80 tours].
[/NODE]

+[NODE]7
[STORY]
Vous découvrez un foyer bourdonnant au cœur duquel s'agitent de nombreux insectes. Cela vous fait quelque peu penser à une grosse ruche d'abeilles, jusqu'à ce que vous remarquiez plusieurs canaux souterrains ainsi que d'autres nids qui semblent tous connectés les uns aux autres afin de former une seule grande colonie.
Malheureusement, vous n'entretenez pas de relations amicales avec les Insectes, lesquels se ruent sur vous !
[/STORY]
[OUT]Se défendre !
[OUT]Courir !
[/NODE]

+[NODE]8
[STORY]
Le repaire des Insectes est détruit, et tous ses habitants sont morts.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]11
[STORY]
La morsure empoisonnée des Insectes s'avère trop virulente, ce qui vous oblige à prendre la fuite.
[/STORY]
[OUT]Partir.
[/NODE]

+[NODE]16
[STORY]
Une reine abeille vous examine :
« Lorsque les ténèbres sont apparues à la surface de la terre il y a de ça longtemps, nous avons été transformés. Aujourd'hui, nous savons comment maîtriser notre corps et notre esprit. Si vous nous offrez de la viande — beaucoup de viande —, nous vous dirons comment faire, camarade. »
[/STORY]
[OUT]Entraîner vos bêtes.
[OUT]Refuser poliment.
[/NODE]

+[NODE]22
[STORY]
Une reine abeille vous examine :
« Lorsque les ténèbres sont apparues à la surface de la terre il y a de ça longtemps, nous avons été transformés. Aujourd'hui, nous savons comment maîtriser notre corps et notre esprit. Si vous nous offrez de la viande — beaucoup de viande —, nous vous dirons comment faire, camarade. »
[/STORY]
[OUT]Oui, accepter.
[OUT]Refuser poliment.
[/NODE]

+[NODE]26
[STORY]
Un petit insecte a l'air d'apprécier votre présence et semble vouloir vous accompagner, mais vous devez d'abord payer un tribut aux Insectes.
[/STORY]
[OUT]L'accueillir dans votre groupe.
[OUT]Refuser et revenir à vos moutons.
[/NODE]

+[NODE]33
[STORY]
Un petit insecte a l'air d'apprécier votre présence et semble vouloir vous accompagner, mais vous devez d'abord payer un tribut aux Insectes.
[/STORY]
[OUT]L'accueillir dans votre groupe.
[OUT]Refuser et revenir à vos moutons.
[/NODE]

[/EVENT]

