## Installation manuelle

Placez le fichier Thea2FR entier dans le répertoire "Thea 2 The Shattering\Thea2_Data\StreamingAssets\Mods"

Démarrez Thea 2, depuis le menu principal cliquez sur Mods, puis activez le mode Thea2FR

Redémarrez le jeu pour profiter de la traduction !