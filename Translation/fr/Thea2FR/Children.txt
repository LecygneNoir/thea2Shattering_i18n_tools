-- [EVENT] --StorkHum(0)
+[NODE]2
[STORY]
Deux membres du groupe profitent d'un peu de bon temps ensemble, de façon amicale... ou plus, qui saurait le dire ?
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkGob(1)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrcMix(2)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrcMix2(3)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrc(4)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkElf(5)
+[NODE]2
[STORY]
Les elfes sont connus pour ne choisir comme compagnons que les meilleurs. Il semblerait qu'un individu particulièrement beau ait tapé dans l’œil de l'un d'entre eux.
Les enfants elfes naissent grâce à un lien mystique, et deux d'entre vous ont eu la chance de connaitre un tel lien.
Les conséquences d'une telle union peuvent varier, mais un enfant arrive bientôt.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkGobMix(6)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkGobMix2(7)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkDwarfRus(8)
+[NODE]2
[STORY]
La fascination que les nains éprouvent pour la beauté sans pareille des rusalkas est bien connue de tous. Et, justement, le nain et la démone aquatique ont décidé de perpétuer ces traditions venues du fond des âges.
La venue d'une cigogne annonce qu'un enfant est issu de cette union. Mais… à qui ressemblera-t-il ? Nul ne le sait.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

+[NODE]5
[STORY]
À l'encontre de ce que nous croyons savoir des us et coutumes naines, un enfant nain, qui est né sans sortir d'un rocher-de-vie, se présente, et vous savez que c'est le vôtre.
[/STORY]
[OUT]L'accueillir.
[/NODE]

+[NODE]7
[STORY]
Les rusalkas sont connues pour attirer les hommes afin de procréer, mais cela n'arrive presque jamais avec les non-humains. Et pourtant, le destin vous sourit, et une jeune rusalka, votre fille, sort du lac.
[/STORY]
[OUT]L'accueillir.
[/NODE]

+[NODE]8
[STORY]
Les rusalkas sont connues pour attirer les hommes afin de procréer, mais cela n'arrive presque jamais avec les non-humains. Et pourtant, le destin vous sourit, et un jeune nébuleur, votre fils et rejeton mâle d'une rusalka, sort du lac.
[/STORY]
[OUT]L'accueillir.
[/NODE]

+[NODE]10
[STORY]
Les rusalkas sont connues pour attirer les hommes afin de procréer, mais cela n'arrive presque jamais avec les non-humains.
De façon surprenante, la rusalka a mis au monde un enfant qui ressemble à un humain, mais qui affiche des traits provenant de ses deux parents.
[/STORY]
[OUT]L'accueillir.
[/NODE]

[/EVENT]

-- [EVENT] --CabbagePatch(9)
+[NODE]2
[STORY]
Deux de vos compagnons trouvent un enfant dans le champ de choux !
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --CabbagePatchMix3(10)
+[NODE]2
[STORY]
Deux de vos compagnons trouvent un enfant dans le champ de choux !
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrcMixDiff5(11)
+[NODE]2
[STORY]
En dépit des importantes différences culturelles, l'amour ne connaît pas de frontières. Et ainsi, sous l’œil vigilant de Lada, un hymne fougueux à l'amour brise la monotonie de la vie.
Vous repérez une cigogne et, bien entendu, l'heureux couple découvre peu de temps après un enfant qui a été déposé pour eux.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrcMix2Diff5(12)
+[NODE]2
[STORY]
En dépit des importantes différences culturelles, l'amour ne connaît pas de frontières. Ainsi, sous l’œil vigilant de Lada, un hymne fougueux à l'amour brise la monotonie de la vie.
Vous repérez une cigogne et, bien entendu, l'heureux couple découvre peu de temps après un enfant qui a été déposé pour eux.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --StorkOrcDiff7(13)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort dans les bras des autres.
Vous repérez une cigogne et, bien entendu, l'heureux couple découvre peu de temps après un enfant qui a été déposé pour eux.
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --__StorkGob(14)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort dans les bras des autres.
Vous repérez une cigogne et, bien entendu, l'heureux couple découvre peu de temps après un enfant qui a été déposé pour eux.
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --__StorkGobMix(15)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --__StorkGobMix2(16)
+[NODE]2
[STORY]
Les nuits sont longues et certains se sentent seuls. Ils vont alors chercher du réconfort auprès des autres.
Vous repérez une cigogne, toujours attentive mais parfois un peu trop zélée. Et bien entendu, peu de temps après, un enfant est déposé à proximité.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --Unliving Mating(17)
+[NODE]2
[STORY]
Soit l'amour n'a pas de frontières, soit la cigogne se comporte étrangement mais...
Votre compagnon mort-vivant est devenu très ami avec un autre membre du groupe, et la cigogne a décidé d'apporter un rejeton pour qu'ils en prennent soin.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --Dwarf Child(18)
+[NODE]2
[STORY]
La façon dont les nains se reproduisent est un secret très bien gardé, et puisque le chemin que vous avez suivi n'est pas celui de votre race, vous ne savez pas comment cela se passe. Cependant, comme vous avez montré votre amitié au Tellurien, vous êtes tous deux invités à pratiquer le rituel du don de vie, ce qui pourrait vous donner un enfant.
[/STORY]
[OUT]Prendre part au rituel.
[/NODE]

+[NODE]5
[STORY]
Vous pénétrez profondément dans les tunnels nains. L'homme doit suivre son instinct pour trouver le rocher-de-vie. Sa compagne attend. Lorsqu'il rapporte le rocher, la femme chante le chant-de-vie, puis c'est ensemble qu'ils sculptent la pierre en espérant qu'elle devienne un enfant.
Une telle cérémonie ne peut avoir lieu qu'une seule fois toutes les deux décennies et elle n'est pas toujours couronnée de succès.
[/STORY]
[OUT]Attendre le résultat.
[OUT]Attendre le résultat.
[/NODE]

+[NODE]6
[STORY]
Bien qu'éloigné de ceux de votre race, votre instinct a prouvé sa force. Votre rocher-de-vie a bien donné un enfant.
[/STORY]
[OUT]Attendre le résultat.
[/NODE]

+[NODE]7
[STORY]
Vous avez été trop longtemps loin des vôtres et votre chant-de-vie n'a pas pu éveiller l'âme d'un nain. À la place, c'est un jeune gravailleur qui vient à la vie.
[/STORY]
[OUT]Attendre le résultat.
[/NODE]

[/EVENT]

-- [EVENT] --Unliving Mating2(19)
+[NODE]2
[STORY]
Soit l'amour n'a pas de frontières, soit la cigogne se comporte étrangement mais...
Votre compagnon mort-vivant est devenu très ami avec un autre membre du groupe, et la cigogne a décidé d'apporter un rejeton pour qu'ils en prennent soin.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --CabbagePatchNonHum(20)
+[NODE]2
[STORY]
Deux de vos compagnons trouvent un enfant dans le champ de choux !
[/STORY]
[OUT]Se réjouir.
[/NODE]

[/EVENT]

-- [EVENT] --__Unliving Mating2(21)
+[NODE]2
[STORY]
Soit l'amour n'a pas de frontières, soit la cigogne se comporte étrangement mais...
Votre compagnon mort-vivant est devenu très ami avec un autre membre du groupe, et la cigogne a décidé d'apporter un rejeton pour qu'ils en prennent soin.
[/STORY]
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[OUT]Se réjouir.
[/NODE]

[/EVENT]

