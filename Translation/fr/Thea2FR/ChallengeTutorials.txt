-- [EVENT] --Tutorial1(0)
+[NODE]2
[STORY]
« Yanek, Yanek ! Viens par ici mon petit ! »
Vous entendez la voix de votre grand-mère provenant de la maison : « Apporte-moi donc un pot de cornichon, mon chéri, ils sont dans la cave. Fais juste attention à ces saletés de rats, ils sont très gros à cette période de l'année. »
Vous récupérez votre épée,  même si un piège à souris serait plus approprié. 
[/STORY]
[OUT]Dire à votre grand-mère que vous y allez tout de suite. Descendre à la cave, prêt à faire face aux rats.
[/NODE]

+[NODE]3
[STORY]
Comme vous avait prévenu votre mamie, il y a des rats ici. Vous hésitez puis entendez la voix rassurante de votre ami Théodore :
« Tu es sur le point de commencer un défi gamin. Qu'est-ce donc qu'un défi, me demanderas-tu ? Et bien, c'est un moyen de résoudre les conflits et problèmes dans ce monde. Quelquefois ce sera un conflit entre ton groupe et celui d'un ennemi, à d'autres moments ce sera une tentative pour résoudre un puzzle ou juste une simple épreuve de force. Les défis se déroulent sous la forme d'une partie de carte. Comme si les dieux eux-mêmes étaient en train de jouer avec notre destin, tu vois. »
[/STORY]
[OUT]Une partie de carte ? Comme un Blackjack normal ?
[/NODE]

+[NODE]6
[STORY]
Tu ne comptes pas abandonner aussi facilement, n'est-ce pas ?
[/STORY]
[OUT]Réessayer une nouvelle fois.
[OUT]S'enfuir et retourner au Menu Principal.
[/NODE]

+[NODE]7
[STORY]
Les rats sont vaincus et les cornichons enfin vôtres. Fier de vous, vous rapportez le pot à votre grand-mère.
« Ta cheville est en train de saigner, Yanek, tu te sens bien ? » demande-t-elle.
Haussant les épaules, vous la rassurez en lui disant que tout va bien. Mais elle vous adresse son habituel regard condescendant :
« Pense donc à prendre une armure avec toi la prochaine fois, mon chéri, avant de finir par t'écorcher les jambes ! Quoi qu'il en soit, s'il te plaît, sois un amour et va me vérifier ce qui attire ces sales vermines dans notre cave, tu veux bien ? Franchement si ton grand-père a encore renversé de la compote de poire je te jure que je... »
[/STORY]
[OUT]Retourner à la cave.
[/NODE]

+[NODE]9
[STORY]
Vous passez la porte de la cave une nouvelle fois, prêt à débarrasser le lieu des cadavres des rats et en finir une bonne fois, quand soudain vous remarquez une ombre bouger. La forme dans l'obscurité d'un coin vous semble familière, bien que plus grosse que d'habitude. Une pomme de pin ? En vous approchant vous discernez une paire d'yeux jaunes et globuleux qui vous toise de haut en bas ainsi qu'une rangée de dents tranchantes vous éblouissant de son sourire.
[/STORY]
[OUT]Serait-ce une titepomdepin ?
[/NODE]

+[NODE]10
[STORY]
Vous vous souvenez vaguement de quelques histoires que vous auriez entendu à propos d’esprits des bois ressemblant à des pommes de pin. Certes ils savent user de magie mais ils tourmentent rarement les humains en dehors des forêts. Vous n'avez pas la moindre idée de ce que cette créature peut bien faire ici, mais il s'agit de la cave de votre grand-mère après tout et nulle pomme de pin ne touchera à ses provisions d'hiver !
Tandis que vous marchez l'épée à la main d'un pas assuré vers la créature pour la faire déguerpir d'ici, les yeux de cette dernière se mettent à briller et soudain un cri strident vous perce les oreilles. Vous resserrez votre prise sur la poignée de votre lame mais constatez qu'elle s'effrite et tombe en poussière.
« Quelle est cette fourberie ? » vous demandez-vous, mais avant que vous n’ayez trouvé la réponse la titepomdepin vous lance une attaque magique. 
[/STORY]
[OUT]Se défendre !
[/NODE]

+[NODE]12
[STORY]
Hum... ça n'était pas supposé se passer comme ça ! Avez-vous triché ? Le but même de ce didacticiel était de vous apprendre le principe de la fuite. Pourquoi êtes-vous venu ici du coup, hein ? Retournez massacrer des orcs et arrêtez de jouer au débutant.
[/STORY]
[OUT]S'en aller...
[/NODE]

+[NODE]13
[STORY]
Vous vous évanouissez, mais êtes rapidement ramené à vous par la voix de Théodore :
« Eh bien, tu l'as un peu cherché, tu crois pas ? Te lancer dans un défi spirituel sans l'aide d'une personne spécialisée dans ce domaine, à quoi pensais-tu ? Faire face à un adversaire beaucoup plus fort que soi n'est pas vraiment une bonne idée. Plutôt que de subir une défaite totale, mieux vaut fuir ou se rendre lorsque les chances sont contre toi , quitte à repartir avec des ecchymoses ou une baisse de moral. T'as intérêt à bien t'en souvenir ! » 
[/STORY]
[OUT]Aah ! Ma tête...
[/NODE]

+[NODE]14
[STORY]
Vous sortez de votre transe hypnotique infligée par la titepomdepin et parvenez à décamper de la cave, en verrouillant la porte derrière vous. Vous vous ruez vers votre grand-mère pour la prévenir de l'invasion de la titepomdepin. Elle hausse un sourcil :
« Qu'est-ce que tu me chantes ? Tu veux dire que tu l'a laissée dans la cave ? Par la force de Svarog... Dire qu'à mon époque, c'est avec ce genre de pins que nous alimentions les feux de nos maisons, les jeunes d'aujourd'hui sont devenus de vrais froussards. D'accord, je m'en occuperai moi-même. Mais après le dîner bien entendu. Bon, j’admets quand même que fuir était très intelligent de ta part plutôt que de te faire botter l'arrière-train. »
[/STORY]
[OUT]Fin
[/NODE]

+[NODE]15
[STORY]
« Rien de tout ça, gamin. Une carte que tu mets en jeu symbolise un personnage : sa force, ses faiblesses et ses compétences. Cette épée que tu tiens, par exemple : tu peux l'utiliser contre ces rats et grâce à cela, acquérir des pouvoirs dans ce jeu de carte. »
[/STORY]
[OUT]Ouf, vous avez vraiment cru pendant une seconde que cette histoire de défis pouvait être quelque chose de dangereux...
[/NODE]

+[NODE]16
[STORY]
« Évidemment que ça l'est, gamin ! Tes blessures lors de tes affrontements sont aussi vraies que ce pot de cornichons que tu comptes récupérer. Je ferais attention à ta place, sinon tu risques de passer tout le reste de la semaine chez un guérisseur ou bien encore de souffrir mentalement ou spirituellement. Alors fais très attention !
Quand tu verras ce symbole : <sprite name=ChallengePhysical> – cela signifiera que tu feras face à un défi physique. La difficulté sera indiquée par un nombre à côté, allant de 1 à 10. »
Vous ne savez pas vraiment comment Théo fait pour aller et venir à volonté, ni quand il décide de le faire, mais peu importe – vous y voilà, debout devant la cave de votre grand-mère, paré à toute éventualité.
[/STORY]
[OUT]Ouvrir la porte et se frayer un chemin jusqu'au pot de cornichons.
[OUT]Repartir, vous avez plus important à faire. [Quitter vers le Menu Principal]
[/NODE]

[/EVENT]

-- [EVENT] --Tutorial2(1)
+[NODE]2
[STORY]
« Merci à vous, braves aventuriers ! », vous dit votre grand-mère, qui fait de son mieux pour parler d'une voix la plus douce et profonde possible, mais celle-ci sonne toujours comme celle du grand méchant loup : « La princesse est sauve. Voici votre carte au trésor. »
Vous vous réjouissez d'un cri aigu et bondissez de joie si fort que vous renversez presque la chaise à bascule de votre mamie. Une fois votre calme retrouvé, elle reprend :
« Les héros examinent de plus près la carte jusqu'à se rendre compte... que les indications sont toutes mélangées. Ils devront donc résoudre cette énigme afin d'obtenir le trésor. »
Elle vous adresse l'un de ses regards  sérieux :
« Mais ça n'est qu'une histoire, n'est-ce pas ? Allez ! Maintenant, au lit vous deux ! »
[/STORY]
[OUT]Attendre que votre grand-mère s'en aille, puis observer la carte.
[/NODE]

+[NODE]3
[STORY]
Théodore s'adresse de nouveau à vous :
« Les défis peuvent aussi jauger les compétences intellectuelles de ton groupe. Mais cette fois, contrairement à ton affrontement contre les rats, tu feras face à un concept.
Le terme « concept » désigne généralement toutes sortes de situations où tu n'affrontes pas un ennemi réel. Escalader une falaise, repérer un danger, résoudre une énigme ou même accomplir un rituel, sont des exemples de défis face à un concept.
Tout comme les personnages, les concepts sont représentés par des cartes, mais lors des défis conceptuels seuls les pouvoirs innés de tes personnages comptent. Ceux apportées par tes équipements seront ignorés. Les règles restent cependant les mêmes.
Tu sauras qu'il s'agit d'un défi conceptuel à chaque fois que tu verras un symbole comme celui-là : <sprite name="MentalConceptChallengeIcon"> . »
[/STORY]
[OUT]D'accord, commencer le Défi mental.
[/NODE]

+[NODE]5
[STORY]
Vous entendez derrière vous les éclats de rire de grand-mère :
« Bien joué, mes petits aventuriers ! Voilà pour vous, votre récompense vous attend sur cette table au milieu de ces champs ! »
Ayant dit cela, elle vous tend un bol rempli de fruits ainsi que votre confiture préférée : celle aux scarabées.
« Allez, au lit maintenant ! Et je ne plaisante plus cette fois ! »
Mais bien évidemment, votre réponse ne se fait pas attendre...
[/STORY]
[OUT]Noooon... Une histoire ! Une histoire ! Encore une s'il te plaaaaît !
[/NODE]

+[NODE]6
[STORY]
Grand-mère soupire. Même en sachant qu'il se fait tard, elle décide de vous donner quelques indices en récompense de votre détermination face à la difficulté de ce défi : vous vous sentez plus intelligents. Elle vous demande ensuite :
« Je sais que tout à l’heure je vous ai dit d’aller vous mettre au lit, mais voulez-vous essayer à nouveau ? »
[/STORY]
[OUT]Oui !
[OUT]Non...
[OUT]S'endormir. [Quitter vers le Menu Principal]
[/NODE]

+[NODE]9
[STORY]
« Très bien, il se fait tard les enfants, c’est l’heure d’aller se coucher », vous dit-elle en refermant son bouquin :
« N’oubliez pas de bien vous laver les pieds ! »
Mais bien évidemment, votre réponse ne se fait pas attendre...
[/STORY]
[OUT]Noooon... Une histoire ! Une histoire ! Encore une s'il te plaaaaît !
[/NODE]

+[NODE]10
[STORY]
Grand-mère inspire profondément, malgré sa fatigue elle est prête à faire respecter son autorité :
« Maintenant ouvrez bien vos petites oreilles, bande de canailles, si vous tenez vraiment à ce que je vous en raconte une autre, il va falloir patienter jusqu'à demain. Sinon les loups et les monstres viendront vous croquer pendant la nuit parce que vous vous êtes mal comportés, ce n'est pas ce que vous voulez, n'est-ce pas ? »
Vous vous regardez l'un l'autre. Cette pensée même vous donne la chair de poule, mais rien que l'idée d'insister jusqu'à faire craquer votre grand-mère vous excite au point d'encourir ce risque.
[/STORY]
[OUT]On veut une histoire ! On veut une histoire ! On veut une histoire !
[/NODE]

+[NODE]12
[STORY]
Alors que vous pensiez l'avoir acculé, la vieille femme sort son dernier atout de sa manche : elle s'adosse à sa chaise, ferme les yeux et se met à ronfler.
Vous parvenez furtivement à lui piquer le livre des mains ; avec un peu de chance vous trouverez une autre histoire intéressante.
Alors que la lune brille à travers votre fenêtre, les hurlements lointains des loups et les bruissements du vent dans la cour parviennent à vos oreilles...
[/STORY]
[OUT]Fin.
[/NODE]

+[NODE]13
[STORY]
Grand-mère n'a pas l'air de plaisanter. Avec ses bras croisés et un sourcil levé, vous savez d'avance que cela ne sert à rien d'insister. Vous vous ruez sagement dans vos lits respectifs.
Alors que la lune brille à travers votre fenêtre, les hurlements lointains des loups et les bruissements du vent dans la cour parviennent à vos oreilles.
[/STORY]
[OUT]Fin.
[/NODE]

[/EVENT]

-- [EVENT] --Tutorial3(2)
+[NODE]2
[STORY]
Vous prenez la route vers votre maison, tout aussi joyeux que bourré après une superbe partie de jeu d'argent, votre bourse pleine à votre ceinture. Le soleil commence à se coucher mais l'air ne s'est pas encore refroidi, donc la longue marche qui vous attend ne vous dérange pas tant que ça.
Il vous faut encore passer un petit bois, mais dès l'instant où vous vous apprêtez à prendre le dernier tournant menant à votre maison, un groupe de bandits surgit des buissons.
Le plus petit d'entre eux vous hurle de sa voix de crécelle : « La bourse ou la vie ! »
[/STORY]
[OUT]Votre fierté de nain en prend un coup face à une telle ignorance. Leur dire de dégager avant qu'ils ne gâchent un peu plus votre bonne humeur.
[/NODE]

+[NODE]3
[STORY]
Ils se regardent les uns les autres avant de reporter leur regard sur votre bourse. 
« On a pas peur des gens de ton espèce, on va t'mettre une bonne raclée ! »
Leur bravoure n'a probablement d'égal que leur stupidité, ou alors ils sont suffisamment désespérés pour tenter le coup.
[/STORY]
[OUT]Combattre !
[/NODE]

+[NODE]5
[STORY]
Vous leur bottez l'arrière-train et les mettez en fuite :
« Pitié ! Pardonnez-nous, sire nain, on vous ennuiera plus dorénavant ! »
C'est en toute sûreté que vous rentrez chez vous, assez confiant pour braver n'importe quel défi que Thea vous imposera. 
[/STORY]
[OUT]Les regarder partir.
[/NODE]

+[NODE]6
[STORY]
Toutes nos félicitations !
Vous avez terminé notre didacticiel et assimilé les mécaniques de base de notre jeu de cartes. N'oubliez pas que vous pouvez revenir ici à tout moment afin de vous souvenir de certains détails qui vous auraient échappé.
Sur ce, il est enfin temps pour vous de faire face au monde de Thea... du moins si vous vous en sentez capable !
[/STORY]
[OUT]Mettre fin au didacticiel et retourner au Menu Principal.
[/NODE]

+[NODE]8
[STORY]
Argh, nain ou pas, ils vous ont violemment mis au au sol !
Vous avez été soigné pour le moment.
Voulez-vous prendre votre revanche ? 
[/STORY]
[OUT]Oui !
[OUT]Non, ils m'ont battu mais au moins, maintenant, je comprends les règles.
[/NODE]

[/EVENT]

