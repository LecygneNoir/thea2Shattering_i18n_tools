#!/bin/bash

# This script is used to extract modules to pot
# then pass the .pot in poedit to merge duplication

if [[ "$#" -ne 1 ]]; then
    echo "Please pass exactly 1 parameter: the txt module"
    exit 1

fi

ORIGINAL_TXT="$1"
POT_DIR="$(pwd)/Modules/pot/"
TEMP_DIR="$(mktemp -d)"
POT=$(basename "${ORIGINAL_TXT%%.txt}.pot")
TEMP_POT="${TEMP_DIR}/${POT}"
POT="${POT_DIR}/${POT}"

# Reinit exiting POT
mkdir -p "${POT_DIR}"
truncate -s 0 "${POT}"

dos2unix "${ORIGINAL_TXT}" &> /dev/null

comment_event=""
comment_node=""
comment_story=""
comment_out=""
event_empty=0
out_incr=1
story_first_line=0
while read -r line; do
    if  [[ ${line} == *"-- [EVENT]"* ]]; then
        comment_event="${line}"
        # At start of event, we set event_empty just in case
        event_empty=1
    elif [[ ${line} == *"+[NODE]"* ]]; then
        comment_node="${line}"
        # At least one node, disable event_empty
        event_empty=0
    elif [[ ${line} == *"[STORY]"* ]]; then
        comment_story="${comment_event}@@${comment_node}@@${line}"
        #Time for new story, write the comment to pot
        echo "#. ${comment_story}" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        story_first_line=1
    elif [[ ${line} == *"[/STORY]"* ]]; then
        # we reach end of story, write the empty msgid and reinit the comment_story
        echo 'msgstr ""' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        comment_story=""
    elif [[ ${line} == *"[OUT]"* ]]; then
        comment_out="${comment_event}@@${comment_node}@@[OUT]${out_incr}"
        # OUT are one line, let's write it to the pot
        echo "#. ${comment_out}" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        # Get rid of special character
        line="${line//'"'/'\"'}"
        echo "msgid \"${line#\[OUT\]}\"" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        echo 'msgstr ""' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        ((out_incr++))
    elif [[ ${line} == *"[/NODE]"* ]]; then
        # We reach end of node, reinit
        comment_node=""
        comment_story=""
        comment_out=""
        out_incr=1
    elif [[ ${line} == *"[/EVENT]"* && ${event_empty} == 1 ]]; then
        # We reach end of event and the event is empty, write special lines
        comment_empty="${comment_event}EMPTY@@[EMPTY]0@@[EMPTY]0"
        echo "#. ${comment_empty}" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        echo 'msgid "[SPECIAL EMPTY EVENT STRING]"' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        echo 'msgstr "[SPECIAL EMPTY EVENT STRING]"' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        # Then reinit
        comment_event=""
        comment_node=""
        comment_story=""
        comment_out=""
        event_empty=0
        out_incr=1
    elif [[ ${line} == *"[/EVENT]"* ]]; then
        # We reach end of event, reinit
        comment_event=""
        comment_node=""
        comment_story=""
        comment_out=""
        out_incr=1
    elif [[ "${comment_story}" != "" && ${story_first_line} == 1 ]]; then
        # If the line has nothing particular, and the comment_story is not empty, it's a story string
        # Get rid of special character
        line="${line//'"'/'\"'}"
        # As the story_fist_line is set, it's the first line of the story, add the msgid
        echo "msgid \"${line}\"" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
        story_first_line=0
    elif [[ ${comment_story} != "" ]]; then
        # We are on a multilines story, write it but without the msgid
        # As developer sometimes forgot empty lines at the end of STORY, skip empty lines
        if [[ "${line}" = "" ]]; then
            continue
        fi
        # Get rid of special character
        line="${line//'"'/'\"'}"
        # Also as this is multiline, we need to add \n to the previous line (no way to detect it sooner...)
        sed -e '$s/\(.*\)"$/\1\\n"/' -i "${TEMP_POT}"
        echo "\"${line}\"" | tr -d '\r' | tr -d '\n' >> "${TEMP_POT}"
        echo >> "${TEMP_POT}"
    else
        # It should be a newline, write a newline
        # If new case are added later, we need to deal with it manually, do not break the file by writing them
        echo "" >> "${TEMP_POT}"
    fi

done < "${ORIGINAL_TXT}" 

# Unify duplicate due to pot syntax
msguniq --no-wrap "${TEMP_POT}" > "${POT}"